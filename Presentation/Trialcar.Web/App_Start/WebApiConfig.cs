﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Trialcar.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            #region DealerCar
            config.Routes.MapHttpRoute("GetCarModel", "api/CommonAPI/GetCarModel/{CarmakerVal}", new { controller = "CommonAPI", action = "GetCarModel", CarmakerVal = RouteParameter.Optional });
            config.Routes.MapHttpRoute("GetCarYear", "api/CommonAPI/GetCarYear/{CarmakerVal}/{carModelVal}",
                    new
                    {
                        controller = "CommonAPI",
                        action = "GetCarYear",
                        CarmakerVal = RouteParameter.Optional,
                        carModelVal = RouteParameter.Optional,
                    });
            config.Routes.MapHttpRoute("Getcartrimprop", "api/CommonAPI/GetCarTrimProp/{CarmakerVal}/{carModelVal}/{CarYearVal}",
                    new
                    {
                        controller = "CommonAPI",
                        action = "GetCarTrimProp",
                        CarmakerVal = RouteParameter.Optional,
                        carModelVal = RouteParameter.Optional,
                        CarYearVal = RouteParameter.Optional,
                    });
            config.Routes.MapHttpRoute("Getpropertybytrim", "api/CommonAPI/Getpropertybytrim/{trimval}", new { controller = "CommonAPI", action = "Getpropertybytrim", trimval = RouteParameter.Optional });
            config.Routes.MapHttpRoute("GetCardetailByvin", "api/CommonAPI/GetCarDetailFromVINAPI/{vinNumber}", new { controller = "CommonAPI", action = "GetCarDetailFromVINAPI", vinNumber = RouteParameter.Optional });

            #endregion

        }
    }
}
