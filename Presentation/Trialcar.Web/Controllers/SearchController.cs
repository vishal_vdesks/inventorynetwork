﻿using Incite.Core.Domain;
using Incite.Core.Infrastructure;
using Incite.Data.Repositories;
using Incite.Services;
using Incite.Services.ViewModels;
using PagedList;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Trialcar.Web.Helper;

namespace Trialcar.Web.Controllers
{
    [Authorize]
    public class SearchController : Controller
    {
        #region  Private Member Variables
        private readonly IDealerCarService _DealerCarService;
        private readonly ICarSearchService _CarSearchService;
        private readonly IPaymentService _paymentService;
        private readonly IUsersService _userService;
       
        #endregion
        #region ctor
        public SearchController(IDealerCarService dealerCarService, ICarSearchService carSearchService
            , IUsersService usersService, IPaymentService paymentService)
        {
            _DealerCarService = dealerCarService;
            _CarSearchService = carSearchService;
            _paymentService = paymentService;
            _userService = usersService;
        }
        #endregion


        private void CheckCurrentUserSubscribedOrNot()
        {
            ViewBag.IsSubscribed = false;

            if (!Infrastructure.Authentication.IsAdmin)
            {
                var currentUser = _userService.GetAllUsers().Where(c => c.Id == Infrastructure.Authentication.CurrentUserId).FirstOrDefault();
                //ViewBag.IsSubscribed = currentUser.IsSubscribed;
                if (string.IsNullOrEmpty(currentUser.StripecustomerId))
                    ViewBag.IsSubscribed = false;
                else
                {
                    Customer customer = new Customer();
                    try
                    {
                        customer = _paymentService.GetCustomer(currentUser.StripecustomerId);
                        ViewBag.IsSubscribed = customer.Subscriptions.Count() > 0;
                    }
                    catch (Exception ex)
                    {
                        ViewBag.IsSubscribed = false;
                    }

                }
            }
        }



        public string GetStripCustomerId
        {
            get
            {
                if (Session["StripeCustomerId"] != null)
                    return Session["StripeCustomerId"].ToString();
                else
                {
                    var currentUser = _userService.GetAllUsers().Where(c => c.Id == Infrastructure.Authentication.CurrentUserId).FirstOrDefault();
                    Session["StripeCustomerId"] = currentUser.StripecustomerId;
                    return currentUser.StripecustomerId;
                }
            }
        }



        // GET: Search
        public ActionResult SearchResult()
        {
            CheckCurrentUserSubscribedOrNot();
            return View();
        }
        [ChildActionOnly]
        public PartialViewResult HomeFilter()
        {
            var model = _CarSearchService.PrepareHomeCareFilter();
            return PartialView(model);
        }
        [ChildActionOnly]
        public PartialViewResult SearchLeftFilter(CarFilterViewModel request)
        {
            var model = _CarSearchService.PrepareHomeCareFilter(request);
            return PartialView(model);
        }
        [ChildActionOnly]
        public PartialViewResult MygarageLeftFilter(CarFilterViewModel request)
        {
            var model = _CarSearchService.PrepareHomeCareFilter(request);
            return PartialView(model);
        }
        [ChildActionOnly]
        public PartialViewResult CarlistSortBy(CarFilterViewModel request)
        {
            var model = _CarSearchService.PrepareSortby(request);
            return PartialView(model);
        }
        #region Car Listing
        [ChildActionOnly]
        public PartialViewResult CarListingView(CarFilterViewModel args, bool IsDealerSearch, int dealerId = 0, bool IsSold = false, int Page = 1)
        {
            IPagedList<DealerCarDetails> lst = null;
            AppUsers appUsers = null;
            appUsers = _DealerCarService.getUserById(dealerId);
            long currentUserId;
            if (dealerId > 0)
            {
                currentUserId = dealerId;
            }
            else
            {
                currentUserId = Infrastructure.Authentication.CurrentUserId ?? default(int);
            }

            ViewBag.IsPaymetList = true;
            var userProfile = _userService.GetUserProfile(Convert.ToInt32(currentUserId));
            var userpaymentList = _paymentService.GetUserPaymentSourceList(userProfile.User.Email);
            if (userpaymentList != null && userpaymentList.Data != null && userpaymentList.Count() > 0)
            {
                ViewBag.IsPaymetList = false;
            }



            var model = _DealerCarService.GetDealerCarListing(currentUserId, args, IsDealerSearch, IsSold);

            lst = model.ToPagedList(Page, (int)SiteEnum.PageSetting.PageSize);
            return PartialView(Tuple.Create(lst, appUsers));
        }
        #endregion
    }
}