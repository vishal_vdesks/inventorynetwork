﻿using Incite.Core.Domain;
using Incite.Data.Repositories;
using Incite.Services;
using Incite.Services.ViewModels;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Trialcar.Web.Helper;

namespace Trialcar.Web.Controllers
{
    public class HomeController : Controller
    {
        #region Private Member Variables
        private readonly IUsersService _usersService;
        private readonly IDealerCarService _dealerCarService;
        private readonly IPaymentService _paymentService;
        private readonly ISubListService _subListService;
        private readonly IContactUsService _contactUsService;

        static private bool m_bStatus = false;
        static private bool m_bStatusPurchase = false;
        static private string m_sPlanId = "";
        static private string stripeCustId = "";
        // private readonly ICountryStateService _countryStateService;

        #endregion Private Member Variables
        #region Ctor
        public HomeController(IUsersService usersService, IDealerCarService CarDetailService
            , IPaymentService paymentService, ISubListService sublistservice
            , IContactUsService contactUsService
            //  , ICountryStateService countryStateService
            )
        {
            _usersService = usersService;
            _dealerCarService = CarDetailService;
            _paymentService = paymentService;
            _subListService = sublistservice;
            _contactUsService = contactUsService;
        }
        #endregion Ctor

        #region Utils

        private void CheckCurrentUserSubscribedOrNot()
        {
            ViewBag.IsSubscribed = false;

            if (Infrastructure.Authentication.IsAdmin)
            {
                ViewBag.IsSubscribed = true;
            }

            if (!Infrastructure.Authentication.IsAdmin)
            {
                var currentUser = _usersService.GetAllUsers().Where(c => c.Id == Infrastructure.Authentication.CurrentUserId).FirstOrDefault();
                //ViewBag.IsSubscribed = currentUser.IsSubscribed;
                if (string.IsNullOrEmpty(currentUser.StripecustomerId))
                    ViewBag.IsSubscribed = false;
                else
                {
                    Customer customer = new Customer();
                    try
                    {
                        customer = _paymentService.GetCustomer(currentUser.StripecustomerId);
                        ViewBag.IsSubscribed = customer.Subscriptions.Count() > 0;
                    }
                    catch (Exception ex)
                    {
                        ViewBag.IsSubscribed = false;
                    }

                }
            }

        }

        private void PopupateCountryState()
        {
            //get only us country
            var country = _usersService.GetCountryById(254);
            // var countries = _usersService.GetAllCountry().OrderBy(x => x.Name);
            var states = _usersService.GetAllState();

            //ViewBag.Countries = countries.Select(x =>
            //{
            //    return new SelectListItem()
            //    {
            //        Text = x.Name,
            //        Value = x.Id.ToString()
            //    };

            //}).ToList();
            var countries = new List<SelectListItem>();
            countries.Add(new SelectListItem { Text = country.Name, Value = country.Id.ToString() });
            ViewBag.Countries = countries;


            ViewBag.States = states.Select(x =>
            {
                return new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.Id.ToString()
                };

            }).ToList();
        }


        #endregion


        [HttpPost]
        [AllowAnonymous]
        public void NewsletterSubscribe(SubList model)
        {
            _subListService.InsertSubList(model);

        }

        public ActionResult Index(bool LogOut = false)
        {
            ViewBag.Status = false;

            if (LogOut)
            {
                ViewBag.Login = false;
            }


            if (Request.IsAuthenticated)
            {
                CheckCurrentUserSubscribedOrNot();
                //ViewBag.IsSubscribed = false;
                //get plans
                if (ViewBag.IsSubscribed == false && !Infrastructure.Authentication.IsAdmin)
                {
                    var plans = _paymentService.GetPlanList();
                    ViewBag.StripePlans = plans;
                    //return RedirectToAction("Purchase", new {planId=0 });
                    return RedirectToAction("ChoosePlan");
                }

            }


            return View();
        }



        public ActionResult About()
        {
            var test = _usersService.GetAllUsers();
            //ViewBag.Message = "Your application description page.";

            return View();
        }
        [HttpGet]
        public ActionResult Contact()
        {
            //_usersService.InsatllRecored();
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ContactUs(ContactUs model)
        {
            ProcessViewModel response = _contactUsService.InsertContactUs(model);

            return new JsonResult() { Data = new { status = response.IsSuccess, message = response.Message }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public ActionResult Services()
        {

            // ViewBag.Message = "Your Service page.";

            return View();
        }

        public ActionResult Faq()
        {

            //ViewBag.Message = "Your Faq page.";

            return View();
        }
        [Authorize]
        public PartialViewResult MiniSearch()
        {
            var model = _dealerCarService.PrepareDealarCarViewModel();
            return PartialView("_FilterSearch", model);
        }

        public ActionResult ChoosePlan()
        {
            //var plans = _paymentService.GetPlanList();
            //ViewBag.StripePlans = plans;
            return View();
        }

        //return RedirectToRoute("DealerSetting");

        //public ActionResult ConfirmationMsg(string confirm_value)
        //{
        //    if (confirm_value == "Yes")
        //        ViewBag.Message = "You Clicked Yes"
        //            ;
        //    else
        //        ViewBag.Message = "You Clicked No";

        //    return View();

        //}

        public ActionResult CardConfirmationMsg()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CardConfirmationMsg(string Ok, string Cancel)
        {
            if (Ok == "Yes")
            {
                m_bStatus = true;
                m_bStatusPurchase = false;
                //return Purchase(m_sPlanId);
                return RedirectToRoute("PlanPurchase");
            }
            else
            {
                m_bStatus = true;
                m_bStatusPurchase = true;

                //string sValue = ViewBag.SelectedPlanId;

                //var model = new CustomerPaymentInfo();
                //model.CountryName = "254";

                //return Purchase(m_sPlanId, null, model, "");

                return RedirectToRoute("PlanPurchase");
            }
        }

        private bool IsOldSubscriber()
        {
            if (!Infrastructure.Authentication.IsAdmin)
            {
                var currentUser = _usersService.GetAllUsers().Where(c => c.Id == Infrastructure.Authentication.CurrentUserId).FirstOrDefault();

                if (string.IsNullOrEmpty(currentUser.StripecustomerId))
                {
                    return false;
                }
                else
                {
                    stripeCustId = currentUser.StripecustomerId;
                }
                //return RedirectToRoute("PlanPurchase");
            }

            return true;
            //return RedirectToRoute("PlanPurchase");
        }

        public void GetCardDetail()
        {

        }

        public ActionResult Purchase(string planId)
        {
            if (planId != null && planId != "pay" && planId != "AddCard")
                m_sPlanId = planId;

            ViewBag.SelectedPlanId = m_sPlanId;
            //ViewBag.IsSuccess = true;
            //ViewBag.ValidUpto = DateTime.Now.AddMonths(1);
            var model = new CustomerPaymentInfo();

            var planInfo = _paymentService.GetPlan(m_sPlanId);
            ViewBag.PlanInfo = planInfo;

            PopupateCountryState();

            model.CountryName = "254";

            ViewBag.InfoModel = model;

            var oldModel = ViewBag.Model;

            if (IsOldSubscriber() && !string.IsNullOrEmpty(stripeCustId))
            {
                var cardService = new CardService();
                var cardOptions = new CardListOptions{};
                var CardDetail = cardService.List(stripeCustId, cardOptions);
                if (CardDetail != null)
                {
                    ViewBag.CardDetail = CardDetail;

                    var s = CardDetail.Data.FirstOrDefault();

                    dynamic d = (dynamic)s;
                    var n = d.CustomerId;

                    var stripeCustomer = _paymentService.GetCustomer(n);
                    ViewBag.StripCustomer = stripeCustomer;
                }
                //   m_bStatus = true;
                //   return RedirectToRoute("OldUserPaymentSourceList");
            }

            if (m_bStatusPurchase)
            {
                m_bStatus = false;
                return Purchase(m_sPlanId, null, model, "");
            }            
            return View(model);

            //if (IsOldSubscriber() && (!m_bStatus))
            //{
            //    return RedirectToRoute("CardConfirmationMsg");
            //    //CardConfirmationMsg();
            //    //return RedirectToRoute("Confirmation");
            //    //return Purchase(planId, null, model, "");
            //}
            ////return RedirectToRoute("PaymentConfirm");
            //else if (m_bStatusPurchase)
            //{
            //    m_bStatus = false;
            //    m_bStatusPurchase = false;
            //    return Purchase(m_sPlanId, null, model, "");
            //}

            //m_bStatusPurchase = false;
            //m_bStatus = false;
        }

        [HttpPost]
        public ActionResult Purchase(string id, FormCollection form, CustomerPaymentInfo model, string IsUpdate = "")
        {
            m_bStatusPurchase = false;
            ViewBag.SelectedPlanId = id;

            if (model == null)
            {
                m_bStatusPurchase = true;
                return RedirectToRoute("PlanPurchase");
            }

            model.AppUserId = Infrastructure.Authentication.CurrentUserId.GetValueOrDefault();

            var planInfo = _paymentService.GetPlan(id);
            ViewBag.PlanInfo = planInfo;

            PopupateCountryState();
            model.CountryName = "254";

            var isReturning = IsOldSubscriber();

            var currentUser = _usersService.GetAllUsers().Where(c => c.Id == model.AppUserId).FirstOrDefault();
            model.CustomerEmailId = currentUser.Email;
            model.Amount = planInfo.Amount.GetValueOrDefault();

            if (string.IsNullOrEmpty(IsUpdate))
            {
                if (ModelState.IsValid)
                {

                    try
                    {
                        var customerResponse = _paymentService.CreateCustomer(model, "card");


                        if (customerResponse.IsSuccess)
                        {
                            var response = _paymentService.CreateSubscription(id, customerResponse.AdditionalData, currentUser, planInfo, isReturning);
                            if (response.IsSuccess && !string.IsNullOrEmpty(response.AdditionalData))
                            {
                                if (planInfo.Nickname == "T16m")
                                {
                                    var couponservice = new CouponService();
                                    var couponOptions = new CouponListOptions { Limit = 5, };

                                    var mycoupon = couponservice.List(couponOptions).Where(x => x.Id == "wrTiagYS");

                                    if (mycoupon != null)
                                    {
                                        var options = new SubscriptionUpdateOptions

                                        {
                                            TrialEnd = DateTime.Now.AddDays(180),
                                            CouponId = mycoupon.FirstOrDefault().Id,
                                            Prorate = false
                                        };
                                        var service = new SubscriptionService();
                                        var subscriptionId = _paymentService.GetCustomer(customerResponse.AdditionalData).Subscriptions.FirstOrDefault().Id;
                                        var subId = subscriptionId;
                                        service.Update(subId, options);
                                    }
                                    else
                                    {
                                        var options = new SubscriptionUpdateOptions

                                        {
                                            TrialEnd = DateTime.Now.AddDays(180),
                                            Prorate = false
                                        };
                                        var service = new SubscriptionService();
                                        var subscriptionId = _paymentService.GetCustomer(customerResponse.AdditionalData).Subscriptions.FirstOrDefault().Id;
                                        var subId = subscriptionId;
                                        service.Update(subId, options);
                                    }

                                }

                                ViewBag.ValidUpto = DateTime.Now.AddMonths((int)planInfo.IntervalCount);
                                ViewBag.IsSuccess = true;
                                return View();
                                //  return RedirectToRoute("PaymentConfirm", new { planId = id });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ModelState.AddModelError("", ex.Message);
                    }

                }
            }
            else
            {
                var customerSubscription = _paymentService.GetCustomer(currentUser.StripecustomerId).Subscriptions.FirstOrDefault();

                var response = _paymentService.CancelSubscription(customerSubscription.Id);
                if (response.IsSuccess)
                {
                    //create new subscription

                    var rr = _paymentService.CreateSubscription(id, currentUser.StripecustomerId, currentUser, planInfo, true);
                    if (rr.IsSuccess && !string.IsNullOrEmpty(rr.AdditionalData))
                    {

                        return RedirectToRoute("DealerSetting");
                    }

                }
            }



            return View();
        }


        public ActionResult PurchaseSuccess(string planId)
        {
            CheckCurrentUserSubscribedOrNot();
            var planInfo = _paymentService.GetPlan(planId);
            ViewBag.PlanInfo = planInfo;

            //if (planInfo.Interval == "month")
            //{
            //    ViewBag.ValidUpto = DateTime.Now.AddMonths((int)planInfo.IntervalCount);
            //}
            //else if (planInfo.Interval == "")
            //{

            //}
            ViewBag.ValidUpto = DateTime.Now.AddMonths((int)planInfo.IntervalCount);

            return View();
        }




        public ActionResult GetStripePlanList(bool isUpdate = false)
        {
            ViewBag.IsUpdate = isUpdate;
            ViewBag.IsOldCustomer = IsOldSubscriber();
            var currentUser = _usersService.GetAllUsers().Where(c => c.Id == Infrastructure.Authentication.CurrentUserId).FirstOrDefault();

            var plans = _paymentService.GetPlanList();

            if (!string.IsNullOrEmpty(currentUser.StripecustomerId))
            {
                //get current subscription
                if (_paymentService.GetCustomer(currentUser.StripecustomerId).Subscriptions.FirstOrDefault() != null)
                {
                    var currentPlan = _paymentService.GetCustomer(currentUser.StripecustomerId).Subscriptions.FirstOrDefault().Plan.Id;
                    ViewBag.EistingPlan = currentPlan;
                }
                //   plans = plans.Where(x => x.Id != currentPlan).ToList();
            }


            ViewBag.StripePlans = plans;
            return PartialView();
        }
    }
}