﻿using System.Web.Mvc;

namespace Trialcar.Web.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
               "Admin_default_Page",
               "Admin/",
               new { controller= "Dashboard",  action = "Index" }
           );
            context.MapRoute(
              "ManageCarObjectIndex",
              "Admin/ManageCarObject/Index",
              new { controller = "ManageCarObject", action = "Index" }
          );
            context.MapRoute(
              "UserIndex",
              "Admin/User/Index",
              new { controller = "User", action = "Index" }
          );
            context.MapRoute(
              "UserUpdateStatus",
              "Admin/User/UpdateUserStatus",
              new { controller = "User", action = "UpdateUserStatus" }
          );
            context.MapRoute(
              "UpdateUserLockoutStatus",
              "Admin/User/UpdateUserLockoutStatus",
              new { controller = "User", action = "UpdateUserLockoutStatus" }
          );
            context.MapRoute(
              "DealerCarIndex",
              "Admin/DealerCar/Index",
              new { controller = "Car", action = "Index" }
          );
        }
    }
}