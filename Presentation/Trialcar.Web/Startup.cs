﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Trialcar.Web.Startup))]
namespace Trialcar.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
