﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Incite.Data.Repositories;
using Incite.Data.Repositories.Impl;
using Incite.Services;
using Incite.Services.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Trialcar.Web.Infrastructure
{
    public class ControllerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            //container.Register(

            //  //All MVC controllers
            //  // Classes.FromThisAssembly().BasedOn<IController>().LifestyleTransient()

            //  Classes.FromThisAssembly()
            //                       .BasedOn<IController>()
            //                        .If(t => t.Name.EndsWith("Controller")).LifestyleTransient()


            //  );

            container.Register(

                //All MVC controllers
                // Classes.FromThisAssembly().BasedOn<IController>().LifestyleTransient(),

                Classes.FromThisAssembly()
                                     .BasedOn<IController>()
                                      .If(t => t.Name.EndsWith("Controller")).LifestyleTransient(),

              Component.For<ICountryRepository, CountryRepository>(),
               Component.For<IStateRepository, StateRepository>(),

                 Component.For<INotificationsRepository, NotificationsRepository>(),
                   Component.For<INotificationsService, NotificationsService>(),

              Component.For<ISubListRepository, SubListRepository>(),
                   Component.For<ISubListService, SubListService>(),

                   Component.For<IContactUsRepository, ContactUsRepository>(),
                   Component.For<IContactUsService, ContactUsService>()


              );

        }
    }
}