﻿$(document).ready(function () {

    $("input:checkbox[id*='compare_chk_']").click(function () {
        var formData = $(this).parent('form').serialize();
        var oChkBox = $(this);

        if ($(this).prop("checked")) {

            $.ajax({
                type: "POST",
                url: "DealerCar/AddToCompare",
                data: formData,
                dataType: "json"
            }).done(function (data) {
                if (data.err) {
                    oChkBox.prop("checked", false);
                }

                if (data.msg) {
                    oChkBox.parents(".product-listing-m").prev(".disp_msg_compare").html(data.msg).removeClass("text-danger").addClass("bg-success");
                }
                else if (data.err) {
                    oChkBox.parents(".product-listing-m").prev(".disp_msg_compare").html(data.err).removeClass("bg-success").addClass("text-danger");
                }
            }).fail(function () {
                oChkBox.parents(".product-listing-m").prev(".disp_msg_compare").html("Fail To Add Car To Compare.").removeClass("bg-success").addClass("text-danger");
            });

        } else {
            $.ajax({
                type: "POST",
                url: "DealerCar/RemoveToCompare",
                data: formData,
                dataType: "json"
            }).done(function (data) {
                if (data.err) {
                    oChkBox.parents(".product-listing-m").prev(".disp_msg_compare").html(data.err).removeClass("bg-success").addClass("text-danger");
                }
                else {
                    oChkBox.prop("checked", false);
                    if (data.msg) {
                        oChkBox.parents(".product-listing-m").prev(".disp_msg_compare").html(data.msg).removeClass("text-danger").addClass("bg-success");
                    }
                }
            }).fail(function () {
                oChkBox.parents(".product-listing-m").prev(".disp_msg_compare").html("Fail To Remove Car From Compare").removeClass("bg-success").addClass("text-danger");
            });
        }
    });
});

function addToCompareSuccess(data) {
    if (data.overLimit)
        $(this).checked = false;
    alert(data.msg);
}

function removeToCompareSuccess(data) {
    $(this).checked = false;
    alert(data.msg);
}