﻿$(function() {
    $(document).unbind('ajaxStart');
    SearchVin = function(This) {
        var EnterVin = $("#txtVin").val();
        if (EnterVin === "" || EnterVin.length < 2) {
            alert("Please Enter VIN");
            return;
        }
        $(This).attr("disabled", true);
        var icon = $(This).find("i");
        //"fa-search"
        $(icon).removeClass("fa-search");
        $(icon).addClass("spin");
        $.ajax({
            type: "GET",
            async: true,
            url: '/api/CommonAPI/GetCarDetailFromVINAPI/' + EnterVin, // we are calling json method
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(Data) {
               
                if (Data.MakerId > 0) {
                    $("#drpCarbrand").val(Data.MakerId);
                    $("#drpCarbrand").change();
                }
                else {
                    ResetForm();
                    alert("No recored Found Please Enter Manually")
                }
                if (Data.ModelId > 0) {
                    setTimeout(function() {
                        $("#drpCarModel").val(Data.ModelId);
                        $("#drpCarModel").change();
                    }, 500)
                }
                if (Data.YearId > 0) {
                    setTimeout(function() {
                        $("#drpmodelyear").val(Data.YearId);
                        $("#drpmodelyear").change();
                    }, 500)
                    $("#drptrimprop").focus();
                }
                if (Data.TrimId > 0) {
                    setTimeout(function() {
                        $("#drptrimprop").val(Data.TrimId);
                        $("#drptrimprop").change();
                    }, 500)
                }
                if (Data.EngineId > 0) {
                    setTimeout(function() {
                        $("#drpEngineType").val(Data.EngineId);
                    }, 500)
                }
                $(This).attr("disabled", false);
                $(icon).addClass("fa-search");
                $(icon).removeClass("spin");
            },
            failure: function() {
                $(This).attr("disabled", false);
                $(icon).addClass("fa-search");
                $(icon).removeClass("spin");
            }
        });
    };
    ResetForm = function() {
        $("#drpCarbrand").val("");
        $("#drpCarbrand").change();
        $("#txtfueltype").val("");
        $("#txtdrivetrain").val("");
        $("#drpEngineType").val("");
        $("#txttransmision").val("");
        $("#txtenginename").val("");
    }

})