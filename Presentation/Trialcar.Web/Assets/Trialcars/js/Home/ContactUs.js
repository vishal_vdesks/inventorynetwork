﻿
$(document).on('click', '#btnSubmitContactUsForm', function (event) {
    event.preventDefault();
   
    var myform = $("#contactUsForm");
    
    if (myform.valid()) {
        $('#btnSubmitContactUsForm').attr('disabled', true);
        $.ajax({
            url: '/Home/ContactUs',  //(rec)= Controller's-name 
            //(recieveData) = Action's method name
            type: 'POST',
            datatype: 'text',
            data: myform.serialize(),
            success: function (data) {
                debugger;
                myform[0].reset();
                $('#btnSubmitContactUsForm').attr('disabled', false);
                var messageDiv = $('#messageDiv'); // get the reference of the div
                messageDiv.attr('style', 'background-color: green !important');
                var message = new String("Thanks For Contacting us! Our Executive will contact you shortly.");

                messageDiv.show().html(message.fontcolor("white")); // show and set the message
                setTimeout(function () { messageDiv.hide().html(''); }, 25000);


            },
            error: function () {
                $('#btnSubmitContactUsForm').attr('disabled', true);
                debugger;
                var messageDiv = $('#messageDiv'); // get the reference of the div
                messageDiv.attr('style', 'background-color: red !important');
                var message = new String(data.message);
                messageDiv.show().html(message.fontcolor("white")); // show and set the message
                setTimeout(function () { messageDiv.hide().html(''); }, 25000);
            }
        });
    }
});
