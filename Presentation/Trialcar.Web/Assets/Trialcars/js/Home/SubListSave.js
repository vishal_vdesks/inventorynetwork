﻿$(document).on('click', '#subBtn', function (event) {
    event.preventDefault();
    var myform = $("#subscribeform");
    if (myform.valid()) {

        debugger;
        $.ajax({
            url: '/Home/NewsletterSubscribe',  //(rec)= Controller's-name 
            //(recieveData) = Action's method name
            type: 'POST',
            timeout: '20000',// (optional 12 seconds)
            datatype: 'text',
            data: {
                //Get the input from Document Object Model
                //by their ID
                EmailId: $('#EmailId').val(),

            },
            success: function () {
                $('#EmailId').val('');

                var messageDiv = $('#message'); // get the reference of the div
                messageDiv.show().text('Subscribed Successfully!'); // show and set the message
                setTimeout(function () { messageDiv.hide().html(''); }, 5000);


             
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                //alert("Status: " + textStatus); alert("Error: " + errorThrown);
            }     

        });
    }
});
