﻿
$(function () {
    $('#drpCarbrand').trigger('change');
    if ($('#drpCarbrand').length > 0 && typeof (DealerCarViewModel) != "undefined") {
        if (DealerCarViewModel.CB  > 0) {
            $('#drpCarModel').val(DealerCarViewModel.CM);
        }
        destroyCurrentModelObject();
    }
});

function SerachView() {

    window.location.href = "/Search/";

};

function destroyCurrentModelObject() {
    delete (DealerCarViewModel);
};
