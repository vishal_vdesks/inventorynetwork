﻿
function readImageURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#profileimg')
                .attr('src', e.target.result)
                .width(225)
                .height(126);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

$(document).on('click', '#btnResetUpdateProfile', function (event) {
    window.location.reload();
});

$(document).on('click', '#btnUpdateProfile', function (event) {
    event.preventDefault();

    var frm = $("#frmUserProfile");
    if (frm.valid()) {
        var fd = new FormData();
        var file_data = $('input[type="file"]')[0].files;
        for (var i = 0; i < file_data.length; i++) {
            fd.append("file_" + i, file_data[i]);
        }
        var other_data = frm.serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });
        $.ajax(
            {
                url: "/Account/UpdateProfile",
                type: "POST",
                data: fd,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data !== undefined && data !== null) {
                        if (data.status) {
                            //frm[0].reset();
                            $(".dvdismiss").show();
                            $(".dvdismiss").html(data.message);
                            $('#lblAdminName').text($('#AdminName').val());
                            $('#lblPhone').text($('#MobileNumber').val());
                            $('#lblAddressline1').text($('#DealershipAddressLine1').val());
                            $('#lblAddressline2').text($('#DealershipAddressLine2').val());
                            // $('#lblEmail').text()
                        }
                        else {
                            // frm[0].reset();
                            $(".dvdismiss").show();
                            $(".dvdismiss").html(data.message)

                        }
                    }
                },
                error: function (data) {
                    $(".dvdismiss").show();
                    $(".dvdismiss").html(data.message)
                }
            });

    }
});

$(document).ready(function () {
    $('#profileimg').mousemove(function () {
        $('#uploadnewlogo').css('display', 'block');
    });

    $('#uploadnewlogo').mouseout(function () {
        $('#uploadnewlogo').css('display', 'none');
    });
})

