﻿
$(document).on('click', '#btnForgotPassword', function (event) {
    event.preventDefault();

    var frm = $("#frmForgotPassword");

    if (frm.valid()) {
        $.ajax(
            {
                url: "/Account/ForgotPassword",
                type: "POST",
                data: frm.serialize(),
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                processData: false,
                success: function (data) {
                    if (data != undefined && data != null) {
                        if (data.status) {
                            frm[0].reset();
                            $(".dvdismiss").show();
                            $(".dvdismiss").html(data.message);
                        }
                        else {
                            frm[0].reset();
                            $(".dvdismiss").show();
                            $(".dvdismiss").html(data.message)

                        }
                    }
                },
                error: function (data) {
                    $(".dvdismiss").show();
                    $(".dvdismiss").html(data.message)
                }
            });
    }
});


