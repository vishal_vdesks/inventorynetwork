﻿$(document).on('click', '#btnLogin', function (event) {
    event.preventDefault();
      var frm = $("#frmLogin");
    if (frm.valid()) {
        $("#load_wrapper").show();
        var fd = new FormData();
        var other_data = frm.serializeArray();
        $.each(other_data, function (key, input) {
            fd.append(input.name, input.value);
        });

        $.ajax(
            {
                url: "/Account/Login",
                type: "POST",
                data: fd,
                contentType: false,
                processData: false,
                success: function (data) {
                       if (data !== undefined && data !== null) {
                        if (data.status) {
                            frm[0].reset();
                            setTimeout(function () {
                                }, 50);
                            window.location.href = "/";
                           
                        }
                        else {
                            $("#load_wrapper").hide();
                            $(".dvdismiss").show();
                            $(".dvdismiss").html(data.message);
                        }
                       }
                  
                },
                error: function (data) {
                    $("#load_wrapper").hide();
                }
            });

    }
  
});

function OnSuccess() {
    $("#load_wrapper").hide();
}
function OnFailure() {
    $("#load_wrapper").hide();
}

