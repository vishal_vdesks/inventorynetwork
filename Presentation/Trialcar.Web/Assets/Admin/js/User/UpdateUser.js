﻿function UpdateUserStatus(userguid, status) {
    $.ajax(
        {
            url: "/Admin/User/UpdateUserStatus",
            type: "POST",
            data: JSON.stringify({ UserGuid: userguid, Status: status }),
            contentType: 'application/json; charset=UTF-8',
            processData: false,
            success: function (data) {
                if (data != undefined && data != null) {
                    if (data.status) {
                        $(".dvdismiss").show();
                        $(".dvdismiss").html(data.message);
                        window.location.reload();
                    }
                    else {
                        $(".dvdismiss").show();
                        $(".dvdismiss").html(data.message);

                    }
                }
            },
            error: function (data) {
                $(".dvdismiss").show();
                $(".dvdismiss").html(data.message)
            }
        });
}

function UpdateUserLockoutStatus(userguid, status) {
    $.ajax(
        {
            url: "/Admin/User/UpdateUserLockoutStatus",
            type: "POST",
            data: JSON.stringify({ UserGuid: userguid, Status: status }),
            contentType: 'application/json; charset=UTF-8',
            processData: false,
            success: function (data) {
                if (data != undefined && data != null) {
                    if (data.status) {
                        $(".dvdismiss").show();
                        $(".dvdismiss").html(data.message);
                        window.location.reload();
                    }
                    else {
                        frm[0].reset();
                        $(".dvdismiss").show();
                        $(".dvdismiss").html(data.message);
                    }
                }
            },
            error: function (data) {
                $(".dvdismiss").show();
                $(".dvdismiss").html(data.message)
            }
        });
}