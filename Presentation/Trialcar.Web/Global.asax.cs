﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Trialcar.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static CacheItemRemovedCallback OnCacheRemove = null;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AddTask();
        }
        private void AddTask()
        {
            OnCacheRemove = new CacheItemRemovedCallback(HoldCarRemoved);
            HttpRuntime.Cache.Insert("UANUpdate", 60, null,
                DateTime.Now.AddSeconds(60), Cache.NoSlidingExpiration,
                CacheItemPriority.NotRemovable, OnCacheRemove);
        }
        public void HoldCarRemoved(string k, object v, CacheItemRemovedReason r)
        {
            try
            {
                using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString()))
                {

                    var command = new SqlCommand("UpdateDealerCarOnHoldStatus", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    connection.Open();
                    try
                    {
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                    }

                    connection.Close();
                }
            }
            catch (Exception ex)
            {

            }

            AddTask();
        }
    }
}
