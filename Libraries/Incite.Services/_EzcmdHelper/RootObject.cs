﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services.EzcmdHelper
{
    public class RootObject
    {
        public bool success { get; set; }
        public int remaining_lookups { get; set; }
        public SearchInfo search_info { get; set; }
        public List<SearchResult> search_results { get; set; }
    }
}
