﻿
using Incite.Core.Domain;
using Incite.Core.Infrastructure;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using System.Web.Helpers;
using System.Web.UI;
using System.IO;

namespace Incite.Services
{
    public class EmailTemplateService : IEmailTemplateService
    {

        public bool ProcessedToEmail(AppUsers model)
        {
            bool result = false;
            try
            {
                string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                string URL = string.Format(SiteConstants.ConfirmEmailURL, baseUrl, model.UserGuid);
                string body = "<br/><br/>We are excited to tell you that your account is" +
                              " successfully created. After verification by admin, you will be able to login in. We will notify you via Email.";
                string SendToEmail = model.Email;
                string SendEmailSubject = "DealerInventoryNetwork Account Confirmation";
                string SendEmailBody = "";
                SendEmailBody += body;
                WebMail.SmtpServer = WebConfigurationManager.AppSettings["mailserver"];
                WebMail.SmtpPort = Convert.ToInt32(WebConfigurationManager.AppSettings["mailPort"]);
                WebMail.EnableSsl = bool.Parse(WebConfigurationManager.AppSettings["mailSSL"]);
                WebMail.UserName = WebConfigurationManager.AppSettings["mailUserName"];
                WebMail.Password = WebConfigurationManager.AppSettings["mailPassword"];
                string FromEmail = WebConfigurationManager.AppSettings["Frommail"];
                WebMail.Send(to: SendToEmail, subject: SendEmailSubject, body: SendEmailBody, from: FromEmail, isBodyHtml: true);
                result = true;
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public bool ProcessedToEmailUserActivate(AppUsers model)
        {
            bool result = false;
            try
            {
                string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                string URL = baseUrl;
                string body = "";
                string SendToEmail = model.Email;
                string SendEmailSubject = "Email Confirmation: DealerInventoryNetwork";
                string SendEmailBody = this.createEmailBody(URL);
                SendEmailBody += body;
                WebMail.SmtpServer = WebConfigurationManager.AppSettings["mailserver"];
                WebMail.SmtpPort = Convert.ToInt32(WebConfigurationManager.AppSettings["mailPort"]);
                WebMail.EnableSsl = bool.Parse(WebConfigurationManager.AppSettings["mailSSL"]);
                WebMail.UserName = WebConfigurationManager.AppSettings["mailUserName"];
                WebMail.Password = WebConfigurationManager.AppSettings["mailPassword"];
                string FromEmail = WebConfigurationManager.AppSettings["Frommail"];
                WebMail.Send(to: SendToEmail, subject: SendEmailSubject, body: SendEmailBody, from: FromEmail, isBodyHtml: true);
                result = true;
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        private string createEmailBody(string URL)

        {

            string SendEmailBody = string.Empty;
            //using streamreader for reading my htmltemplate   

            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("/MailTemplate/ProcessedToEmailUserActivate.html")))

            {

                SendEmailBody = reader.ReadToEnd();

            }
            SendEmailBody = SendEmailBody.Replace("{URL}", URL);
            return SendEmailBody;

        }

        public bool ProcessedToEmailForgotPassword(AppUsers model)
        {
            bool result = false;
            try
            {
                string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                string URL = string.Format(SiteConstants.ForgotPasswordEmailURL, baseUrl, model.UserGuid);
                string body = "<br/><br/> Please click on the below link to reset your account password" +
                              " <br/><br/><a href='" + URL + "'>" + "Reset Password" + "</a> ";
                string SendToEmail = model.Email;
                string SendEmailSubject = "Password Reset: DealerInventoryNetwork";
                string SendEmailBody = "";
                SendEmailBody += body;
                WebMail.SmtpServer = WebConfigurationManager.AppSettings["mailserver"];
                WebMail.SmtpPort = Convert.ToInt32(WebConfigurationManager.AppSettings["mailPort"]);
                WebMail.EnableSsl = bool.Parse(WebConfigurationManager.AppSettings["mailSSL"]);
                WebMail.UserName = WebConfigurationManager.AppSettings["mailUserName"];
                WebMail.Password = WebConfigurationManager.AppSettings["mailPassword"];
                string FromEmail = WebConfigurationManager.AppSettings["Frommail"];
                WebMail.Send(to: SendToEmail, subject: SendEmailSubject, body: SendEmailBody, from: FromEmail, isBodyHtml: true);
                result = true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex);
            }
            return result;
        }

    }
}
