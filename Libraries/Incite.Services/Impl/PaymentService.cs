﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Incite.Core.Domain;
using Incite.Core.Infrastructure;
using Incite.Data;
using Incite.Data.Repositories;
using Incite.Services.ViewModels;
using Stripe;
using Trialcar.Web.Helper;
using static Incite.Core.Infrastructure.SiteEnum;
using System.Web.Configuration;

namespace Incite.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly ICustomerPaymentTypesRepository _customerPaymentTypesRepository;
        private readonly IDealerConnectCreationRepository _dealerConnectCreationRepository;
        private readonly IDealerCustomerCreationRepository _dealerCustomerCreationRepository;
        private readonly IAppUsersRepository _appUsersRepository;
        private readonly IDealerCarDetailsRepository _dealerCarDetailsRepository;
        private readonly ICarOrderDetailRepository _carOrderDetailRepository;
        private readonly ICarOrderOnHoldRepository _carOrderOnHoldRepository;
        private readonly IUserProfileRepository _userProfileRepository;

        public const string COUNTRY = "US";
        public const string CURRENCY = "usd";
        //visahl key
        //public const string TOKEN = "sk_test_xWMuJcfS74rDNiTQvqcSP71s";
        //client key
        // public const string TOKEN = "sk_test_mKzyuApPVcvY6egwzQIBzryb";

        //vishal new key
        public const string TOKEN = "sk_test_abD0ZjXMoaBhKiTf9Ut81dHo";


        #region ctor
        public PaymentService(ICustomerPaymentTypesRepository CustomerPaymentTypesRepository
            , IDealerConnectCreationRepository DealerConnectCreationRepository
            , IDealerCustomerCreationRepository DealerCustomerCreationRepository
            , IAppUsersRepository appUsersRepository
            , IDealerCarDetailsRepository DealerCarDetailsRepository
            , ICarOrderDetailRepository CarOrderDetailRepository
            , ICarOrderOnHoldRepository CarOrderOnHoldRepository
            , IUserProfileRepository userProfileRepository)

        {
            _customerPaymentTypesRepository = CustomerPaymentTypesRepository;
            _dealerConnectCreationRepository = DealerConnectCreationRepository;
            _dealerCustomerCreationRepository = DealerCustomerCreationRepository;
            _appUsersRepository = appUsersRepository;
            _dealerCarDetailsRepository = DealerCarDetailsRepository;
            _carOrderDetailRepository = CarOrderDetailRepository;
            _carOrderOnHoldRepository = CarOrderOnHoldRepository;
            _userProfileRepository = userProfileRepository;
        }
        #endregion


        #region Stripe's Create/Get Customer Account
        [UnitOfWork]
        public ProcessViewModel CreateCustomer(UpdateSettingsViewModel model, string stripeCustomerId = "")
        {
            ProcessViewModel result = new ProcessViewModel();
            result.IsSuccess = false;
            result.Message = SiteConstants.Bankaccount_Failure;
            if (model.AppUserId <= 0)
            {
                return result;
            }
        //    if(stripeCustomerId == "" && model.Userprofile.User.StripecustomerId == "") { 
            StripeConfiguration.SetApiKey(TOKEN);
            var custservice = new CustomerService();
            var Customeroptions = new CustomerListOptions { Email = model.CustomerEmailId };
            StripeList<Customer> customers = custservice.List(Customeroptions);
         //       }
            //if (customers.Data.Count() > 0)
            //{
            //    return result;
            //}

            try
            {
                BankAccountService bankAccountService = new BankAccountService();
                var bbank = bankAccountService.Create(stripeCustomerId, new BankAccountCreateOptions
                {
                    SourceBankAccount = new SourceBankAccount
                    {
                        AccountHolderName = model.AccountHolderName,
                        AccountHolderType = "company",
                        RoutingNumber = model.RoutingNumber,
                        AccountNumber = model.AccountNumber,
                        Country = COUNTRY,
                        Currency = CURRENCY
                    }
                });

                var appuser = _appUsersRepository.Get(model.AppUserId);
                DealerCustomerCreation dealerCustomerCreation = new DealerCustomerCreation();
                dealerCustomerCreation.CustomerId = stripeCustomerId;
                dealerCustomerCreation.IsVerified = false;
                dealerCustomerCreation.User = appuser;

                CustomerPaymentTypes paymentTypes = new CustomerPaymentTypes();
                paymentTypes.PaymentId = bbank.Id;
                paymentTypes.PaymentType = CustomerPaymentType.BankAccount.ToString();
                paymentTypes.StripeId = bbank.Id;
                paymentTypes.DealerCustomerCreation = dealerCustomerCreation;

                _customerPaymentTypesRepository.Insert(paymentTypes);
                _dealerCustomerCreationRepository.Insert(dealerCustomerCreation);
                result.IsSuccess = true;
                result.Message = SiteConstants.Bankaccount_Success;

            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }


            var options = new TokenCreateOptions
            {
                BankAccount = new BankAccountOptions
                {
                    Country = COUNTRY,
                    Currency = CURRENCY,
                    AccountHolderName = model.AccountHolderName,
                    AccountHolderType = "company",
                    RoutingNumber = model.RoutingNumber,
                    AccountNumber = model.AccountNumber,
                },
                CustomerId = stripeCustomerId

            };

            var service = new TokenService();
            //Token stripeToken = service.Create(options);
            ////check if strip is created bank account or not if is not then token not comming 
            //// we reqiered token for add customer
            //if (stripeToken.StripeResponse != null)
            //{
            //    var appuser = _appUsersRepository.Get(model.AppUserId);
            //    DealerCustomerCreation dealerCustomerCreation = new DealerCustomerCreation();
            //    dealerCustomerCreation.CustomerId = stripeCustomerId;
            //    dealerCustomerCreation.IsVerified = false;
            //    dealerCustomerCreation.User = appuser;

            //    CustomerPaymentTypes paymentTypes = new CustomerPaymentTypes();
            //    paymentTypes.PaymentId = stripeToken.BankAccount.Id;
            //    paymentTypes.PaymentType = CustomerPaymentType.BankAccount.ToString();
            //    paymentTypes.StripeId = stripeCustomerId;
            //    paymentTypes.DealerCustomerCreation = dealerCustomerCreation;

            //    _customerPaymentTypesRepository.Insert(paymentTypes);
            //    _dealerCustomerCreationRepository.Insert(dealerCustomerCreation);
            //    result.IsSuccess = true;
            //    result.Message = SiteConstants.Bankaccount_Success;

            //}


            ////var service = new TokenService();
            ////Token stripeToken = service.Create(options);
            //////check if strip is created bank account or not if is not then token not comming 
            ////// we reqiered token for add customer
            ////if (stripeToken.StripeResponse != null)
            ////{
            ////    var Custoptions = new CustomerCreateOptions
            ////    {
            ////        Description = model.Description,
            ////        SourceToken = stripeToken.Id,
            ////        Email = model.CustomerEmailId

            ////    };
            ////    Customer customer = custservice.Create(Custoptions);
            ////    var appuser = _appUsersRepository.Get(model.AppUserId);
            ////    DealerCustomerCreation dealerCustomerCreation = new DealerCustomerCreation();
            ////    dealerCustomerCreation.CustomerId = customer.Id;
            ////    dealerCustomerCreation.IsVerified = false;
            ////    dealerCustomerCreation.User = appuser;

            ////    CustomerPaymentTypes paymentTypes = new CustomerPaymentTypes();
            ////    paymentTypes.PaymentId = customer.DefaultSourceId;
            ////    paymentTypes.PaymentType = CustomerPaymentType.BankAccount.ToString();
            ////    paymentTypes.StripeId = customer.DefaultSourceId;
            ////    paymentTypes.DealerCustomerCreation = dealerCustomerCreation;

            ////    _customerPaymentTypesRepository.Insert(paymentTypes);
            ////    _dealerCustomerCreationRepository.Insert(dealerCustomerCreation);
            ////    result.IsSuccess = true;
            ////    result.Message = SiteConstants.Bankaccount_Success;

            ////}
            return result;
        }

        public Customer GetCustomer(string stripeCustomerId)
        {
            var customerService = new CustomerService();
            StripeConfiguration.SetApiKey(TOKEN);

            var customer = customerService.Get(stripeCustomerId);
            return customer;
        }

        [UnitOfWork]
        public ProcessViewModel CreateCustomer(CustomerPaymentInfo model, string accountType = "card")
        {
            ProcessViewModel result = new ProcessViewModel();
            result.IsSuccess = false;

            if (model.AppUserId <= 0)
            {
                return result;
            }

            var appuser = _appUsersRepository.Get(model.AppUserId);

            StripeConfiguration.SetApiKey(TOKEN);
            var custservice = new CustomerService();
            var Customeroptions = new CustomerListOptions { Email = model.CustomerEmailId };
            StripeList<Customer> customers = custservice.List(Customeroptions);

            if (customers.Data.Count() > 0)
            {
                result.IsSuccess = true;
                result.AdditionalData = customers.Data.FirstOrDefault().Id;
                if (string.IsNullOrEmpty(appuser.StripecustomerId))
                    appuser.StripecustomerId = result.AdditionalData;
                return result;
            }


            var options = new TokenCreateOptions();

            if (accountType.ToLower() == "bank")
            {
                result.Message = SiteConstants.Bankaccount_Failure;

                options.BankAccount = new BankAccountOptions
                {
                    Country = COUNTRY,
                    Currency = CURRENCY,
                    AccountHolderName = model.AccountHolderName,
                    AccountHolderType = "company",
                    RoutingNumber = model.RoutingNumber,
                    AccountNumber = model.AccountNumber,
                    
                    
                };
            }
            else
            {
                result.Message = SiteConstants.CreditCard_Failure;

                //**##1##**
                var metaData = new Dictionary<string, string>();
                metaData.Add("IsDefault", true.ToString());

                options.Card = new CreditCardOptions
                {
                    Currency = CURRENCY,
                    Cvc = model.CVVNumber,
                    ExpMonth = Convert.ToInt32(model.ExpiryMonth),
                    ExpYear = Convert.ToInt32(model.ExpiryYear),
                    Number = model.CardNumber,
                    Name = model.NameOnCard,
                    AddressCountry = COUNTRY,
                    MetaData = metaData,
                    
                };
            }


            var service = new TokenService();
            Token stripeToken = service.Create(options);
            //check if strip is created bank account or not if is not then token not comming 
            // we reqiered token for add customer
            if (stripeToken.StripeResponse != null)
            {
                var Custoptions = new CustomerCreateOptions
                {
                    Description = model.Description,
                    SourceToken = stripeToken.Id,
                    Email = model.CustomerEmailId

                };
                Customer customer = custservice.Create(Custoptions);

                appuser.StripecustomerId = customer.Id;

                DealerCustomerCreation dealerCustomerCreation = new DealerCustomerCreation();
                dealerCustomerCreation.CustomerId = customer.Id;
                dealerCustomerCreation.IsVerified = true;
                dealerCustomerCreation.User = appuser;

                CustomerPaymentTypes paymentTypes = new CustomerPaymentTypes();
                paymentTypes.PaymentId = customer.DefaultSourceId;
                paymentTypes.PaymentType = CustomerPaymentType.CreditCard.ToString();
                paymentTypes.StripeId = customer.DefaultSourceId;
                dealerCustomerCreation.AppUserId = appuser.Id;

                paymentTypes.DealerCustomerCreation = dealerCustomerCreation;


                _customerPaymentTypesRepository.Insert(paymentTypes);
                _dealerCustomerCreationRepository.Insert(dealerCustomerCreation);


                //add card here

                var cs = new CardService();

                try
                {

                    //CARD ALREADY CREATED AT TOP. SEE **##1##**

                    //NO NEED TO CHARGE HERE, IT WILL GET AUTOMATICALLY CHARGED AT THE TIME OF SUBSCRIPTION
                    //var charge = new ChargeService();
                    //var chargeResponse = charge.Create(new ChargeCreateOptions
                    //{
                    //    Amount = model.Amount,
                    //    Capture = true,
                    //    CustomerId = customer.Id,
                    //    Currency = CURRENCY,
                    //    ReceiptEmail = appuser.Email,
                    //    SourceId = stripeToken.Card.Id
                    //});


                }
                catch (Exception ex)
                {

                }



                result.IsSuccess = true;
                result.AdditionalData = customer.Id;
                if (accountType.ToLower() == "bank")
                {
                    result.Message = SiteConstants.Bankaccount_Success;
                }
                else
                {
                    result.Message = SiteConstants.Card_Success;
                }

            }
            return result;
        }


        public ProcessViewModel UpdateCustomerDefaultPaymentSource(Customer customer, string cardId)
        {
            StripeConfiguration.SetApiKey(TOKEN);
            var cs = new CustomerService();
            var result = new ProcessViewModel();
            try
            {
                cs.Update(customer.Id, new CustomerUpdateOptions { DefaultSource = cardId });
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }
            return result;
        }

        [UnitOfWork]
        public ProcessViewModel VerifyCustomer(string custId, int AppUserId, VerifyBankAccount model)
        {
            ProcessViewModel result = new ProcessViewModel();
            result.IsSuccess = false;
            result.Message = SiteConstants.Bankverify_Failure;

            //DealerCustomerCreation dealerCustomerCreation = _dealerCustomerCreationRepository.Get(x => x.User != null && x.User.Id == AppUserId && x.CustomerId == custId);
            DealerCustomerCreation dealerCustomerCreation = _dealerCustomerCreationRepository.GetAll(x => x.User != null && x.User.Id == AppUserId && x.CustomerId == custId && x.IsVerified == false).FirstOrDefault();
            CustomerPaymentTypes customerPaymentTypes = _customerPaymentTypesRepository.Get(x => x.DealerCustomerCreation != null && x.DealerCustomerCreation.Id == dealerCustomerCreation.Id);
            StripeConfiguration.SetApiKey(TOKEN);
            var options = new BankAccountVerifyOptions
            {
                Amounts = new long[]
                        {
                        model.VerifyDebitAmount,
                        model.VerifyCreditAmount
                        }.ToList()
            };

            var service = new BankAccountService();

            BankAccount bankAccount = service.Verify(
             dealerCustomerCreation.CustomerId,
              customerPaymentTypes.PaymentId,
              options
            );

            if (bankAccount.Status == "verified")
            {
                dealerCustomerCreation.IsVerified = true;
                customerPaymentTypes.IsVerified = true;
                _dealerCustomerCreationRepository.Update(dealerCustomerCreation);
                _customerPaymentTypesRepository.Update(customerPaymentTypes);
                result.IsSuccess = true;
                result.Message = SiteConstants.Bankverify_Success;
                return result;
            }

            return null;
        }
        public StripeList<IPaymentSource> GetUserPaymentSourceList(string UserEmail)
        {
            StripeList<IPaymentSource> result = new StripeList<IPaymentSource>();
            StripeConfiguration.SetApiKey(TOKEN);
            var custservice = new CustomerService();

            var Customeroptions = new CustomerListOptions { Email = UserEmail };
            StripeList<Customer> customers = custservice.List(Customeroptions);
            if (customers != null && customers.Data.Count() > 0 && customers.FirstOrDefault() != null && customers.FirstOrDefault().Sources != null)
            {
                result = customers.FirstOrDefault().Sources;
            }
            return result;
        }
        #endregion

        #region Stripe's Create/Get Connect Account
        [UnitOfWork]
        public ProcessViewModel CreateConnect(AddConnectAccountsViewModel model)
        {
            ProcessViewModel result = new ProcessViewModel();
            result.IsSuccess = false;
            result.Message = SiteConstants.Bankaccount_Failure;
            if (model.AppUserId <= 0)
            {

                return result;
            }
            string[] arr = {"","Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado",
                "Connecticut", "Delaware", "District of Columbia", "Florida", "Georgia",
                "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine",
                "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada",
                "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota",
                "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Puerto Rico", "Rhode Island", "South Carolina", "South Dakota",
                "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming" };
            var appuser = _appUsersRepository.Get(model.AppUserId);
            var profile = _userProfileRepository.Get(x => x.User == appuser);
            StripeConfiguration.SetApiKey(TOKEN);
            try
            {
                
                var options = new AccountCreateOptions
                {
                    Email = model.CustomerEmailId,
                    Type = AccountType.Custom,
                    Country = COUNTRY,
                    // RequestedCapabilities = new List<string> { "card_payments" },
                    RequestedCapabilities = new List<string> { "card_payments", "transfers"},
                    BusinessProfile = new AccountBusinessProfileOptions { Mcc = "5511", Name = model.AccountHolderName, SupportEmail = model.CustomerEmailId, SupportPhone = model.PhoneNumber, SupportUrl= model.URL, ProductDescription = "Dealers Buy and Sell vehicles to one another" },
                    BusinessType = "company",
                    Company = new AccountCompanyOptions
                    { 
                        Name = model.AccountHolderName,
                        TaxId = model.EIN,
                        OwnersProvided = true,
                        DirectorsProvided = false,
                        Phone = model.PhoneNumber,
                        Address = new AddressOptions
                        {
                            City = model.AddressCity,
                            Line1 = model.AddressLine1,
                            Line2 = model.AddressLine2,
                            Country = COUNTRY,
                            State = arr[model.State],
                            PostalCode = model.AddressPostalCode
                        },

                    },
                                        
                    DefaultCurrency = "usd",
                    
                    TosAcceptance = new AccountTosAcceptanceOptions
                    {
                        Date = DateTime.Now,
                        Ip = model.IP
                    },
                    Settings = new AccountSettingsOptions
                    {
                        Payouts = new AccountSettingsPayoutsOptions
                        {
                            Schedule = new AccountSettingsPayoutsScheduleOptions
                            {
                                DelayDays = "2",
                            },
                            DebitNegativeBalances = true,
                            StatementDescriptor = "DIN"
                        },
                        CardPayments = new AccountSettingsCardPaymentsOptions
                        {
                            DeclineOn = new AccountSettingsDeclineOnOptions
                            {
                                AvsFailure = false,
                                CvcFailure = true
                            },
                            StatementDescriptorPrefix = "DIN"
                        },
                        Payments = new AccountSettingsPaymentsOptions
                        {
                            
                        },
                        Branding = new AccountSettingsBrandingOptions
                        {
                            
                        },
                        

                    }
                };
                var service = new AccountService();


                

                Account account = service.Create(options);
                model.AccountId = account.Id;

                System.Diagnostics.Debug.WriteLine(account.ToJson().ToString());

                var personService = new PersonService();
                
                var person = new PersonCreateOptions
                {
                    Address = new AddressOptions
                    {
                        City = model.AddressCity,
                        Line1 = model.AddressLine1,
                        Line2 = model.AddressLine2,
                        Country = COUNTRY,
                        State = arr[model.State],
                        PostalCode = model.AddressPostalCode
                    },
                    Dob = new DobOptions
                    {
                        Day = model.Dob2.Day,
                        Month = model.Dob2.Month,
                        Year = model.Dob2.Year,
                    },
                    
                    Email = model.CustomerEmailId,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Phone = model.PhoneNumber,
                    Relationship = new PersonRelationshipOptions
                    {
                        Title = model.JobTitle,
                        AccountOpener = true,
                        Owner = true,
                        Director = true
                    },
                    SSNLast4 = model.SSNLast4,
                    
                };
                personService.Create(model.AccountId, person);


            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return result;

            }
            try
            {
                var ExAccountOption = new ExternalAccountCreateOptions()
                {
                    ExternalAccountBankAccount = new AccountBankAccountOptions
                    {
                        Country = COUNTRY,
                        Currency = CURRENCY,
                        AccountHolderName = model.AccountHolderName,
                        AccountHolderType = model.AccountHolderType,
                        RoutingNumber = model.RoutingNumber,
                        AccountNumber = model.AccountNumber,
                    },

                };
                var eaService = new ExternalAccountService();
                var cExBankAccount = eaService.Create(model.AccountId, ExAccountOption);


                DealerConnectCreation dealerConnectCreation;
                var currentacount = _dealerConnectCreationRepository.Get(x => x.User == appuser);
                if (currentacount != null)
                    dealerConnectCreation = currentacount;
                else
                {
                    dealerConnectCreation = new DealerConnectCreation();
                    dealerConnectCreation.User = appuser;
                }
                dealerConnectCreation.ActId = cExBankAccount.AccountId;
                dealerConnectCreation.ExternalId = cExBankAccount.Id;

                UpdateConnectAccount(dealerConnectCreation, model);
                dealerConnectCreation.IsVerified = true;
                _dealerConnectCreationRepository.InsertOrUpdate(dealerConnectCreation);

            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                System.Diagnostics.Debug.WriteLine(ex.ToString());
                return result;
            }

            //if success entry in table
            result.IsSuccess = true;
            result.Message = SiteConstants.Bankaccount_Success;

            return result;
        }


        private bool UpdateConnectAccount(DealerConnectCreation dealerConnectCreation, AddConnectAccountsViewModel model)
        {
           



            return true;
        }

        public StripeList<IExternalAccount> GetConnectUserPaymentSourceList(string UserEmail)
        {
            StripeList<IExternalAccount> result = new StripeList<IExternalAccount>();
            StripeConfiguration.SetApiKey(TOKEN);
            var acctservice = new AccountService();

            StripeList<Account> connects = acctservice.List(null, null);
            if (connects != null && connects.Data != null && connects.Data.Count() > 0 && connects.Data.Any(x => x.Email == UserEmail) && connects.Data.FirstOrDefault(x => x.Email == UserEmail).ExternalAccounts != null)
            {
                return connects.Data.FirstOrDefault(x => x.Email == UserEmail).ExternalAccounts;

            }
            return null;
        }
        #endregion

        #region Common Methods
        public List<CustomerPaymentTypes> GetCustomerPaymentTypes(int BuyerId)
        {
            DealerCustomerCreation dealerCustomerCreation = _dealerCustomerCreationRepository.Get(x => x.User != null && x.User.Id == BuyerId);

            var test = _dealerCustomerCreationRepository.GetAll(c => c.User != null && c.User.Id == BuyerId).ToList();


            var customerPaymentTypes = _customerPaymentTypesRepository.GetAll(x => x.DealerCustomerCreation != null && x.DealerCustomerCreation.Id == dealerCustomerCreation.Id).ToList();
            if (customerPaymentTypes != null)
                return customerPaymentTypes;
            return null;
        }


        public List<IPaymentSource> GetCustomerPaymentTypes(string stripeCustomerId)
        {
            var custservice = new CustomerService();
            StripeConfiguration.SetApiKey(TOKEN);
            var paymentTypes = custservice.Get(stripeCustomerId).Sources.ToList();
            return paymentTypes;
        }

        [UnitOfWork]
        public ProcessViewModel ProcessBuyNowHoldNow(BuyNowHoldNowDetailViewModel model)
        {
            ProcessViewModel result = new ProcessViewModel();
            result.IsSuccess = false;
            result.Message = SiteConstants.BankPayment_Failure;


            var DealerCarDtls = _dealerCarDetailsRepository.Get(x => x.Id == model.DealerCarDetailId);
            if (DealerCarDtls == null)
                return result;

            decimal totalPayment, partialPayment = 0, restPayment = 0;
            int timeToOnHoldCar = 0;
            int amount;
            if (model.IsOnHold) //For OnHold Process--partial payment
            {
                //call from web setting value
                var partialPaymentPerc = int.Parse(WebConfigurationManager.AppSettings["OnHoldPaymentPerc"]);
                timeToOnHoldCar = int.Parse(WebConfigurationManager.AppSettings["TimeToOnHoldCar"]);

                //calculate partial and pending amount 
                totalPayment = DealerCarDtls.Price;
                partialPayment = (totalPayment * partialPaymentPerc) / 100;
                restPayment = totalPayment - partialPayment;
                amount = Convert.ToInt32(partialPayment);
            }
            else ////For BuyNow Process--full payment
            {
                amount = Convert.ToInt32(DealerCarDtls.Price);
            }
            if (amount <= 0)
                return result;

            model.SellerId = DealerCarDtls.User.Id;
            var BuyerCustomer = _dealerCustomerCreationRepository.Get(x => x.User != null && x.User.Id == model.BuyerId);
            var SellerDetails = _dealerConnectCreationRepository.Get(x => x.User != null && x.User.Id == model.SellerId);

            var sellerInformation = _dealerCustomerCreationRepository.Get(x => x.User != null && x.User.Id == model.SellerId);

            if (BuyerCustomer == null || SellerDetails == null)
                return result;


            #region Stripe's Payment Related
            StripeConfiguration.SetApiKey(TOKEN);

            //int PaymentTypeId;
            //int.TryParse(model.PaymentTypeId.ToString(), out PaymentTypeId);

            //var payment = BuyerCustomer.CustomerPaymentTypes.FirstOrDefault(x => x.Id == PaymentTypeId);
            //if (payment == null)
            //    return result;

            if (string.IsNullOrEmpty(model.StripePaymentId))
                return result;
            decimal buyercharge = 0;
            decimal sellerdeposite = 0;

            Charge charge;
            try
            {

                if (!model.IsOnHold)
                {
                    decimal transactionFee = (amount * 0.5M / 100) + 5;
                    decimal totalAmount = (transactionFee * 100) + (amount * 100);
                    buyercharge = totalAmount/100;
                    sellerdeposite = amount - (long)transactionFee;
                    var options1 = new ChargeCreateOptions
                    {
                        Amount = (long)totalAmount,
                        StatementDescriptor = "DIN",
                        Currency = CURRENCY,
                        //SourceId = payment.PaymentId,
                        SourceId = model.StripePaymentId,
                        CustomerId = BuyerCustomer.CustomerId,   
                        TransferData = new ChargeTransferDataOptions
                        {
                            Amount = (amount - (long)transactionFee) * 100,
                            Destination = SellerDetails.ActId
                        }
                       
                    };

                    var service1 = new ChargeService();
                    charge = service1.Create(options1);
                    
                }
                else
                {
                    decimal transactionFee = (partialPayment * 5M / 100);
                    if (transactionFee < 5M)
                    {
                        transactionFee = 5M;
                    }
                    decimal totalAmount = partialPayment + transactionFee;
                    decimal sellerAmount = partialPayment - transactionFee;
                    buyercharge = totalAmount;
                    sellerdeposite = sellerAmount;
                    var options1 = new ChargeCreateOptions
                    {
                        Amount = (long)totalAmount * 100L,
                        // ApplicationFeeAmount = (long)((totalExtraPrice * 100)),
                        StatementDescriptor = "DIN",
                        Currency = CURRENCY,
                        //SourceId = payment.PaymentId,
                        SourceId = model.StripePaymentId,
                        CustomerId = BuyerCustomer.CustomerId,
                        TransferData = new ChargeTransferDataOptions
                        {                           
                            Amount = ((long)sellerAmount) * 100,
                            Destination = SellerDetails.ActId
                        },
                        
                    };

                    var service1 = new ChargeService();
                    charge = service1.Create(options1);
                }



            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                return result;
            }

            //#region Balance Fund
            //double AvailableAmount = 0;
            //var bService = new BalanceService();
            //var balance = bService.Get();
            //if (balance != null)
            //{
            //    double converttoammount = 0.0;
            //    if (balance.Available.FirstOrDefault().Amount > 0)
            //    {

            //        converttoammount = Math.Round((double)(balance.Available.FirstOrDefault().Amount) / 100, 2, MidpointRounding.AwayFromZero);

            //    }
            //    AvailableAmount = converttoammount;

            //    if (Convert.ToDouble(amount) >= AvailableAmount)
            //        return result;

            //}
            //#endregion

            //try
            //{
            //    var SellerDetails = _dealerConnectCreationRepository.Get(x => x.User != null && x.User.Id == model.SellerId);
            //    var options = new TransferCreateOptions
            //    {
            //        Amount = amount,
            //        Currency = CURRENCY,
            //        Destination = SellerDetails.ActId
            //    };
            //    var service = new TransferService();
            //    Transfer Transfer = service.Create(options);
            //}
            //catch (Exception ex)
            //{
            //    return result;
            //}

            #endregion


            #region Invoices

            #region Buyer

            var options = new InvoiceItemCreateOptions
            {
                Amount = (amount * 100),
                Currency = CURRENCY,
                CustomerId = BuyerCustomer.CustomerId,
                Description = "Car Purchase",


            };
            var service = new InvoiceItemService();
            var invoiceItem = service.Create(options);

            var ioptions = new InvoiceCreateOptions
            {
                CustomerId = BuyerCustomer.CustomerId,
                Billing = Billing.SendInvoice,
                DaysUntilDue = 30,
            };
            var iservice = new InvoiceService();
            var invoice = iservice.Create(ioptions);


            var sendOptions = new InvoiceSendOptions { };
            var sservice = new InvoiceService();
            Invoice newinvoice = sservice.SendInvoice(invoice.Id, sendOptions);

            #endregion


            #region Seller
            if (sellerInformation != null)
            {
                var seller_options = new InvoiceItemCreateOptions
                {
                    Amount = (amount * 100),
                    Currency = CURRENCY,
                    CustomerId = sellerInformation.CustomerId,
                    Description = "Car Purchase",


                };
                var seller_service = new InvoiceItemService();
                var seller_invoiceItem = service.Create(seller_options);

                var seller_ioptions = new InvoiceCreateOptions
                {
                    CustomerId = sellerInformation.CustomerId,
                    Billing = Billing.SendInvoice,
                    DaysUntilDue = 30,
                };
                var seller_iservice = new InvoiceService();
                var seller_invoice = iservice.Create(seller_ioptions);


                var seller_sendOptions = new InvoiceSendOptions { };
                var seller_sservice = new InvoiceService();
                Invoice seller_newinvoice = sservice.SendInvoice(seller_invoice.Id, seller_sendOptions);
            }


            #endregion

            #endregion

            DealerCarDtls.IsSold = !model.IsOnHold;
            DealerCarDtls.IsOnHold = model.IsOnHold;
            _dealerCarDetailsRepository.Update(DealerCarDtls);


            CarOrderDetail carOrderDetail = new CarOrderDetail();
            CarOrderOnHold carOrderOnHold = new CarOrderOnHold();

            var buyerUser = _appUsersRepository.Get(x => x.Id == model.BuyerId);

            String str = model.IsOnHold ? "Hold" : "Purchase";
            carOrderDetail = new CarOrderDetail
            {
                BuyerCharge = buyercharge,
                SellerDeposit = sellerdeposite,
                PaymentStatus = Convert.ToInt32(charge.Paid),
                TransactionId = charge.Id,
                TransactionType = str, 
                IsOnHold = model.IsOnHold,
                SellerDealer = DealerCarDtls.User,
                BuyerDealer = buyerUser,
                dealerCarDetails = DealerCarDtls,
                CreatedOn = DateTime.Now,
                Amount = amount
            };

            _carOrderDetailRepository.Insert(carOrderDetail);

            if (model.IsOnHold)
            {
                carOrderOnHold = new CarOrderOnHold
                {
                    TimeToHold = DateTime.UtcNow.AddDays(timeToOnHoldCar),
                    PartialAmountPaid = buyercharge,
                    RestAmount = amount - buyercharge,
                    carOrderDetail = carOrderDetail
                };

                _carOrderOnHoldRepository.Insert(carOrderOnHold);
            }
            result.IsSuccess = true;
            result.Message = SiteConstants.BankPayment_Success;

            return result;
        }

        #endregion




        #region Stripe's Plan List
        [UnitOfWork]
        public StripeList<Plan> GetPlanList()
        {
            StripeList<Plan> result = new StripeList<Plan>();
            StripeConfiguration.SetApiKey(TOKEN);
            var planService = new PlanService();


            //SubscriptionService sc = new SubscriptionService();
            //var option = new SubscriptionCreateOptions();
            //option.

            StripeList<Plan> plans = planService.List();

            return plans;
        }

        [UnitOfWork]
        public Plan GetPlan(string Id)
        {
            StripeConfiguration.SetApiKey(TOKEN);
            var planService = new PlanService();
            return planService.Get(Id);
        }





        [UnitOfWork]
        public ProcessViewModel CreateSubscription(string planId, string customerId, AppUsers user, Plan planInfo, bool isReturning)
        {
            var createSubscriptionOption = new SubscriptionCreateOptions() { };
            bool endtrial = false;
            if (isReturning)
            {
                endtrial = true;
                createSubscriptionOption = new SubscriptionCreateOptions()
                {
                    CustomerId = customerId,
                    EndTrialNow = endtrial,
                    Items = new List<SubscriptionItemOption> { new SubscriptionItemOption { PlanId = planId, Quantity = 1 } },
                };
            }
            else
            {
                createSubscriptionOption = new SubscriptionCreateOptions()
                {
                    CustomerId = customerId,
                    TrialPeriodDays = planInfo.TrialPeriodDays,
                    Items = new List<SubscriptionItemOption> { new SubscriptionItemOption { PlanId = planId, Quantity = 1 } },
                };
            }
            SubscriptionService sc = new SubscriptionService();
            StripeConfiguration.SetApiKey(TOKEN);
            var response = sc.Create(createSubscriptionOption);

            var tempUser = _appUsersRepository.Get(user.Id);
            tempUser.IsSubscribed = true;
            tempUser.SubscriptionId = response.Id;
            tempUser.PlanId = planId;
            tempUser.SubscribedUpto = response.BillingCycleAnchor.Value.Date;
            //_appUsersRepository.Update(user);

            var result = new ProcessViewModel();
            result.IsSuccess = true;
            result.Message = "Ok";
            result.AdditionalData = response.Id;
            return result;
        }


        public ProcessViewModel AddCreditCard(CustomerPaymentInfo model, string stripecustomerId)
        {
            var options = new TokenCreateOptions();

            var result = new ProcessViewModel();

            options.Card = new CreditCardOptions
            {
                Currency = CURRENCY,
                Cvc = model.CVVNumber,
                ExpMonth = Convert.ToInt32(model.ExpiryMonth),
                ExpYear = Convert.ToInt32(model.ExpiryYear),
                Number = model.CardNumber,
                Name = model.NameOnCard,
                AddressCountry = COUNTRY,

            };
            //options.CustomerId = stripecustomerId;

            try
            {
                var service = new TokenService();
                Token stripeToken = service.Create(options);
                //check if strip is created bank account or not if is not then token not comming 
                // we reqiered token for add customer
                if (stripeToken.StripeResponse != null)
                {
                    CardService cs = new CardService();
                    try
                    {
                        var cardCreateResponse = cs.Create(stripecustomerId, new CardCreateOptions
                        {
                            SourceToken = stripeToken.Id
                        });

                        result.IsSuccess = !string.IsNullOrEmpty(cardCreateResponse.Id);
                    }
                    catch (Exception ex)
                    {
                        result.Message = ex.Message;
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }


            return result;

        }



        public Card GetCard(string stripeCustomerId, string cardId)
        {
            StripeConfiguration.SetApiKey(TOKEN);
            var cardService = new CardService();
            var card = cardService.Get(stripeCustomerId, cardId);

            return card;
        }

        public ProcessViewModel UpdateCard(CustomerPaymentInfo model, string stripeCustmerId, string cardId)
        {

            var result = new ProcessViewModel();

            var cs = new CardService();
            Card updateResponse = new Card();
            try
            {
                updateResponse = cs.Update(stripeCustmerId, cardId, new CardUpdateOptions { ExpMonth = Convert.ToInt64(model.ExpiryMonth), ExpYear = Convert.ToInt64(model.ExpiryYear) });
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }
            return result;
        }

        public ProcessViewModel DeleteCard(string stripeCustmerId, string cardId)
        {
            var result = new ProcessViewModel();

            var cs = new CardService();
            Card updateResponse = new Card();
            try
            {
                updateResponse = cs.Delete(stripeCustmerId, cardId);
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }
            return result;
        }

        public ProcessViewModel CancelSubscription(string subscriptionId)
        {
            var result = new ProcessViewModel();
            try
            {
                SubscriptionService ss = new SubscriptionService();
                var subscription = ss.Get(subscriptionId);

                var Option = new SubscriptionUpdateOptions
                {
                    CancelAt = subscription.BillingCycleAnchor.Value
                };
                ss.Update(subscriptionId, Option);
               // var cancelOptions = new SubscriptionCancelOptions
               // {
               //     InvoiceNow = false,
               //     Prorate = false,
               // };
               // ss.Cancel(subscriptionId, cancelOptions);
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }
            return result;
        }

        public ProcessViewModel ReSubscription(string subscriptionId)
        {
            var result = new ProcessViewModel();
            try
            {
                SubscriptionService ss = new SubscriptionService();
                var subscription = ss.Get(subscriptionId);

                var Option = new SubscriptionUpdateOptions
                {
                    CancelAt = null
                };
                ss.Update(subscriptionId, Option);
                // var cancelOptions = new SubscriptionCancelOptions
                // {
                //     InvoiceNow = false,
                //     Prorate = false,
                // };
                // ss.Cancel(subscriptionId, cancelOptions);
                result.IsSuccess = true;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
            }
            return result;
        }

        public StripeList<Charge> GetCharges(string stripeCustomerId)
        {
            StripeConfiguration.SetApiKey(TOKEN);
            var chargeService = new ChargeService();

            var charges = chargeService.List(new ChargeListOptions { CustomerId = stripeCustomerId });

            return charges;
        }
        #endregion


    }
}
