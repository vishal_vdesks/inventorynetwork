﻿using Incite.Core.Domain;
using Incite.Core.Infrastructure;
using Incite.Data;
using Incite.Data.Repositories;
using Incite.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services.Impl
{
    public class SubListService :ISubListService
    {
        private readonly ISubListRepository _subListRepository;

        public SubListService(ISubListRepository subListsRepository)
        {
            _subListRepository = subListsRepository;
        }

        public List<SubList> GetAllSubList()
        {
            var query = this._subListRepository.GetAll();

            return query.ToList();
        }

        [UnitOfWork]
        public void InsertSubList(SubList subList)
        {
            if (IsEmailUnique(subList.EmailId))
            {
                this._subListRepository.Insert(subList);
            }
        }

        public bool IsEmailUnique(string email)
        {
            var sublist = this.GetAllSubList();
            foreach (var item in sublist)
            {
                if (item.EmailId.ToLower() == email.ToLower())
                {
                    return false;
                }
               
            }
            return true;
        }
    }
}
