﻿using Incite.Core.Domain;
using Incite.Data;
using Incite.Data.Repositories;
using Incite.Services.Infrastructure;
using Incite.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using Incite.Core.Interfaces;
using Incite.Core.Infrastructure;
using Incite.Core.Extention;
using Incite.Services.Impl;

namespace Incite.Services
{
    public class UsersService : IUsersService
    {
        #region Private Member Variables
        private readonly IAppUsersRepository _appUsersRepository;
        private readonly IAppUsersRoleRepository _appUsersRoleRepository;
        private readonly IUserProfileRepository _userProfileRepository;
        private readonly IEmailTemplateService _emailTemplateService;
        private readonly IDealerCarService _dealerCarService;


        private readonly ICountryRepository _countryRepository;
        private readonly IStateRepository _stateRepo;
        private readonly ICarOrderDetailRepository _orderDetailRepository;
        private readonly ICarOrderOnHoldRepository _orderonHoldRepository;


        #endregion Private Member Variables
        #region Ctor
        public UsersService(IAppUsersRepository appUsersRepository, IAppUsersRoleRepository appUsersRoleRepository, IUserProfileRepository userProfileRepository, IEmailTemplateService emailTemplateService, IDealerCarService dealerCarService
            , ICountryRepository countryRepository
            , IStateRepository stateRepository
            , ICarOrderDetailRepository carOrderDetailRepository
            , ICarOrderOnHoldRepository carOrderOnHoldRepository
            )
        {
            _appUsersRepository = appUsersRepository;
            _appUsersRoleRepository = appUsersRoleRepository;
            _userProfileRepository = userProfileRepository;
            _emailTemplateService = emailTemplateService;
            _dealerCarService = dealerCarService;
            _countryRepository = countryRepository;
            _stateRepo = stateRepository;
            _orderDetailRepository = carOrderDetailRepository;
            _orderonHoldRepository = carOrderOnHoldRepository;
        }
        #endregion Ctor


        // [UnitOfWork]
        public List<Country> GetAllCountry()
        {
            var query = _countryRepository.GetAll();
            return query.ToList();
        }

        public Country GetCountryById(int Id)
        {

            var query = _countryRepository.Get(Id);
            return query;

        }

        // [UnitOfWork]
        public List<State> GetAllState()
        {
            return _stateRepo.GetAll().ToList();
            // return new List<State>();
        }




        #region Public Method
        [UnitOfWork]
        public List<AppUsers> GetAllUsers()
        {
            return _appUsersRepository.GetAll().ToList();
        }

        //public void InsatllRecored()
        //{
        //    var userinrole = _appUsersRoleRepository.Get(1);
        //    var totaluseres = _appUsersRepository.GetAll().Count();
        //    var lasttoatal = totaluseres + 100;
        //    for (int i = totaluseres; i <= lasttoatal; i++)
        //    {
        //        AppUsers user = new AppUsers();
        //        user.Email = "user" + i.ToString();
        //        user.UserName = "user" + i.ToString();
        //        _appUsersRepository.Insert(user);

        //    }


        //}

        /// <summary>
        /// Public method which is used to create new user
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [UnitOfWork]
        public ProcessViewModel NewUser(RegisterViewModel model)
        {
            ProcessViewModel result = new ProcessViewModel();
            string ImagePath = string.Empty;
            string bucketName = "dinpicturebucket";
            AWS3Helper aWS3Helper = new AWS3Helper();
            try
            {
                var getemail = _appUsersRepository.Get(x => x.Email == model.EmailAddress);
                if (getemail != null)
                {
                    result.IsSuccess = false;
                    result.Message = SiteConstants.DealerExist;
                    return result;
                }
                AppUsers newuser = new AppUsers();
                UserProfile userprofile = new UserProfile();

                AppUsersRole userrole = _appUsersRoleRepository.Get(x => x.RoleName == MyExtentionMethods.GetEnumName((int)SiteEnum.Roles.Dealer, typeof(SiteEnum.Roles)));
                if (userrole == null)
                {
                    userrole = new AppUsersRole();
                    userrole.RoleName = MyExtentionMethods.GetEnumName((int)SiteEnum.Roles.Dealer, typeof(SiteEnum.Roles));
                    _appUsersRoleRepository.Insert(userrole);
                }
                newuser.UserName = model.DealershipName;
                newuser.Email = model.EmailAddress;



                newuser.UsersRoles.Add(userrole);
                newuser.Password = model.Password;
                newuser.IsActive = false;
                newuser.IsResetPassword = true;




                string Username = model.DealershipName ?? string.Empty;
                if (!string.IsNullOrEmpty(Username))
                {
                    #region User Profile with Image Upload
                    if (model.UploadProfilePicture != null && model.UploadProfilePicture.ContentLength > 0)
                    {
                        var filename2 = model.LicenseNumber;
                      //  var fileName = Path.GetFileName(model.UploadProfilePicture.FileName);
                        string folderPath = @"~\UploadedFiles\ProfilePic\" + Username;


                        //Check whether Directory (Folder) exists.
                       // if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(folderPath)))
                       // {
                            //If Directory (Folder) does not exists. Create it.
                            //Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(folderPath));
                        //}

                        aWS3Helper.createFolder(bucketName, string.Empty, "UploadedFiles");
                        aWS3Helper.createFolder(bucketName, @"/UploadedFiles", "ProfilePic");
                        aWS3Helper.createFolder(bucketName, @"/UploadedFiles/ProfilePic", Username);
                       // model.UploadProfilePicture.SaveAs(Path.Combine(System.Web.HttpContext.Current.Server.MapPath(folderPath), filename2));
                        ImagePath = string.Concat(folderPath, @"\", Path.GetFileName(filename2));

                        aWS3Helper.uplaodFile(bucketName + @"/UploadedFiles/ProfilePic/" + Username, filename2 + "Profile_Pic.jpeg", model.UploadProfilePicture.InputStream);
                    }

                    #endregion

                    userprofile = new UserProfile()
                    {
                        AdditionalEmail = model.AdditionalEmailAdress,
                        AdditionalMobileNumber = model.AdditionalMobileNumber,
                        FAX = model.FAX,
                        CreateDate = DateTime.UtcNow,
                        FirstName = model.DealershipName,
                        LastName = string.Empty,
                        State = model.State,
                        MobileNumber = model.MobileNumber,
                        ProfileImage = ImagePath,
                        ProfileThmbnailImage = ImagePath,
                        DealershipAddressLine1 = model.DealershipAddressLine1,
                        DealershipAddressLine2 = model.DealershipAddressLine2,
                        Zipcode = model.ZIPCODE,
                        City = model.City,
                        AdminName = model.AdminName,
                        LicenseNumber = model.LicenseNumber

                    };

                    _appUsersRepository.Insert(newuser);
                    userprofile.User = newuser;
                    _userProfileRepository.Insert(userprofile);
                    var getEncodePassword = CryptoServiceProvider.EncodePassword(newuser.Password, newuser.Id.ToString());
                    newuser.Password = getEncodePassword;
                    _appUsersRepository.Update(newuser);
                    var sendemail = _emailTemplateService.ProcessedToEmailUserActivate(newuser);
                    // bool isUserRegisteredWithChimpMail = EmailSender.RegisterWithMailChimp(model.UserEmail, model.Username);
                    result.IsSuccess = true;
                    result.Message = SiteConstants.Registration_Success;
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
                //AppLogProvider.AppErrorLog(ex);
            }
            return result;
        }

        /// <summary>
        /// Public method which is used to validate user
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        [UnitOfWork]
        public UserViewModel ValidateUser(string email, string Password)
        {
            UserViewModel result = new UserViewModel();
            try
            {
                var user = _appUsersRepository.Get(x => x.Email == email);
                if (user != null)
                {
                    string encodedPassword = CryptoServiceProvider.EncodePassword(Password, user.Id.ToString());
                    if (encodedPassword != user.Password)
                    {
                        result.Process.IsSuccess = false;
                        result.Process.Message = "Invalid Username or Password";
                    }
                    else if (!user.IsEmailConfirmed)
                    {
                        result.Process.IsSuccess = false;
                        result.Process.Message = "Please check you inbox for confirmation email. If confirmed, We will validate your license shortly";

                    }
                    else if (!user.IsLockout)
                    {
                        result.Process.IsSuccess = false;
                        result.Process.Message = "Validation Pending. We will validate your account shortly";
                    }
                    else
                    {
                        //if (user.LastLoginDate == null)
                        //{
                        //    OverlayManager.IsUserFirstTimelogin = true;
                        //}

                        //user.LastLoginDate = DateTime.UtcNow;
                        //_appUsersRepository.Update(user);

                        result.Process.IsSuccess = true;
                        result.user = user;
                    }
                }
                else
                {
                    result.Process.IsSuccess = false;
                    result.Process.Message = "User not available";
                }
            }
            catch (Exception ex)
            {
                //AppLogProvider.AppErrorLog(ex);
            }
            return result;
        }

        [UnitOfWork]
        public bool ConfirmEmail(Guid uid)
        {
            bool status = false;
            try
            {
                var userdetail = _appUsersRepository.Get(x => x.UserGuid == uid);
                if (userdetail != null)
                {
                    userdetail.IsEmailConfirmed = true;
                    userdetail.IsActive = true;
                    _appUsersRepository.Update(userdetail);
                    status = true;
                }
            }
            catch (Exception ex)
            {
            }
            return status;
        }

        #region Update User Profile

        /// <summary>
        /// Based on login user id get UserProfie Details
        /// </summary>
        /// <param name="currentUserId">User Id</param>
        /// <returns>Return UserProfileviewModel</returns>
        [UnitOfWork]
        public UserProfileViewModel GetUserProfile(int currentUserId)
        {
            UserProfileViewModel result = new UserProfileViewModel();
            try
            {
                //Get UserProfile

                #region Old Syntax
                //var userProfile = _appUsersRepository.Get(currentUserId).UserProfile; 
                #endregion
                var user = _appUsersRepository.Get(x => x.Id == currentUserId);
                var userProfile = _userProfileRepository.Get(x => x.User == user);

                //Fill result
                result.FirstName = userProfile.FirstName;
                result.LastName = userProfile.LastName;
                result.MobileNumber = userProfile.MobileNumber;
                result.AdditionalMobileNumber = userProfile.AdditionalMobileNumber;
                result.AdditionalEmail = userProfile.AdditionalEmail;
                result.FAX = userProfile.FAX;
                result.AdminName = userProfile.AdminName;
                result.State = userProfile.State;
                if (!userProfile.ProfileImage.Contains("dinpicturebucket.s3.amazonaws"))
                    result.ProfileImage = @"http://dinpicturebucket.s3.amazonaws.com" + userProfile.ProfileImage.Replace("~", "");
                else result.ProfileImage = userProfile.ProfileImage.Replace("~", "");
                if (!userProfile.ProfileThmbnailImage.Contains("dinpicturebucket.s3.amazonaws.com"))
                    result.ProfileThmbnailImage = @"http://dinpicturebucket.s3.amazonaws.com" + userProfile.ProfileThmbnailImage.Replace("~", "");
                else result.ProfileThmbnailImage = userProfile.ProfileThmbnailImage.Replace("~", "");
                result.User = userProfile.User;
                result.DealershipAddressLine1 = userProfile.DealershipAddressLine1;
                result.DealershipAddressLine2 = userProfile.DealershipAddressLine2;
                result.LicenseNumber = userProfile.LicenseNumber;
                result.Zipcode = userProfile.Zipcode;
                result.City = userProfile.City;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public AppUsers GetAppUser(int appuserId)
        {
            return _appUsersRepository.Get(appuserId);
        }

        /// <summary>
        /// Update User profile
        /// </summary>
        /// <param name="model">UserProfileViewModel</param>
        /// <param name="currentUserId">Logged in user id</param>
        /// <returns>Return ProcessViewModel (Success/Failure - status and Message)</returns>
        [UnitOfWork]
        public ProcessViewModel UpdateUserProfile(UserProfileViewModel model, int currentUserId)
        {
            //Declare object and variable
            ProcessViewModel result = new ProcessViewModel();
            string ImagePath = string.Empty;
            string bucketName = "dinpicturebucket";
            AWS3Helper aWS3Helper = new AWS3Helper();

            //Get UserProfile
            var user = _appUsersRepository.Get(currentUserId);
            var userProfile = _userProfileRepository.Get(x => x.User == user);
            try
            {
                #region User Profile with Image Upload
                if (model.UploadProfilePicture != null && model.UploadProfilePicture.ContentLength > 0)
                {
                    var fileName = Path.GetFileName(model.UploadProfilePicture.FileName);
                    string folderPath = @"~\UploadedFiles\ProfilePic\" + userProfile.User.UserName;

                    //Check whether Directory (Folder) exists.
                    if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(folderPath)))
                    {
                        //If Directory (Folder) does not exists. Create it.
                        Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(folderPath));
                    }
                    aWS3Helper.createFolder(bucketName, string.Empty, "UploadedFiles");
                    aWS3Helper.createFolder(bucketName, @"/UploadedFiles", "ProfilePic");
                    aWS3Helper.createFolder(bucketName, @"/UploadedFiles/ProfilePic", userProfile.User.UserName);
                    model.UploadProfilePicture.SaveAs(Path.Combine(System.Web.HttpContext.Current.Server.MapPath(folderPath), fileName));
                    ImagePath = string.Concat(folderPath, @"\", Path.GetFileName(fileName));

                    aWS3Helper.uplaodFile(bucketName + @"/UploadedFiles/ProfilePic/" + userProfile.User.UserName, fileName, model.UploadProfilePicture.InputStream);
                }
                else
                {
                    ImagePath = userProfile.ProfileImage;
                }

                #endregion

                #region Replace Update value from model with existing value to update UserProfile

                userProfile.AdminName = model.AdminName;
                userProfile.AdditionalEmail = model.AdditionalEmail;
                userProfile.MobileNumber = model.MobileNumber;
                userProfile.AdditionalMobileNumber = model.AdditionalMobileNumber;
                userProfile.ProfileImage = @"http://dinpicturebucket.s3.amazonaws.com" + ImagePath;
                userProfile.ProfileThmbnailImage = @"http://dinpicturebucket.s3.amazonaws.com" + ImagePath;
                userProfile.DealershipAddressLine1 = model.DealershipAddressLine1;
                userProfile.DealershipAddressLine2 = model.DealershipAddressLine2;
                userProfile.FAX = model.FAX;
                userProfile.State = model.State;
                userProfile.City = model.City;
                userProfile.Zipcode = model.Zipcode;
                userProfile.LicenseNumber = model.LicenseNumber;

                #endregion

                //Call Update funtion 
                _userProfileRepository.Update(userProfile);

                //Send Notification Email
                //  var sendemail = _emailTemplateService.ProcessedToEmail(userProfile.User);

                //Result - ProcessviewModel
                result.IsSuccess = true;
                result.Message = string.Format(SiteConstants.Update_Success, "Profile");
            }
            catch (Exception ex)
            {
                //Result - ProcessviewModel
                result.IsSuccess = false;
                result.Message = string.Format(SiteConstants.Update_Failure, "Profile");
            }
            return result;
        }

        #endregion Update User Profile

        #region Update password

        /// <summary>
        /// Update User Password
        /// </summary>
        /// <param name="model">UpdatePasswordViewModel</param>
        /// <param name="currentUserId">currentUserId - Used to pass as a params (uniqueSalt) when generate Encoded Password </param>
        /// <returns>ProcessViewModel (Success/Failure - Status and Message)</returns>
        [UnitOfWork]
        public ProcessViewModel UpdatePassword(UpdatePasswordViewModel model, int currentUserId)
        {
            //Declare Object 
            ProcessViewModel result = new ProcessViewModel();

            try
            {
                //Get User
                var user = _appUsersRepository.Get(currentUserId);
                if (user != null)
                {
                    //Create Encoded password
                    string encodedPassword = CryptoServiceProvider.EncodePassword(model.OldPassword, user.Id.ToString());
                    if (encodedPassword != user.Password)
                    {
                        //Result - ProcessViewModel
                        result.IsSuccess = false;
                        result.Message = "Invalid Old Password";
                    }
                    else if (model.OldPassword == model.NewPassword)
                    {
                        //Result - ProcessViewModel
                        result.IsSuccess = false;
                        result.Message = "New Password should not be same as Old Password.";
                    }
                    else
                    {
                        //Replace Update Password with existing Password
                        user.Password = CryptoServiceProvider.EncodePassword(model.NewPassword, user.Id.ToString());

                        //Call Update function
                        _appUsersRepository.Update(user);

                        //Result - ProcessViewModel
                        result.IsSuccess = true;
                        result.Message = string.Format(SiteConstants.Update_Success, "Password");
                    }
                }
                else
                {
                    //Result - ProcessViewModel
                    result.IsSuccess = false;
                    result.Message = "User not available";
                }
            }
            catch (Exception ex)
            {
                //Result - ProcessViewModel
                result.IsSuccess = true;
                result.Message = string.Format(SiteConstants.Update_Failure, "Password");
            }
            return result;
        }


        [UnitOfWork]
        public ProcessViewModel UpdateUser(AppUsers u)
        {
            ProcessViewModel result = new ProcessViewModel();

            try
            {
                //Get User
                var user = _appUsersRepository.Get(u.Id);
                if (user != null)
                {
                    user.SubscriptionId = u.SubscriptionId;
                    user.IsSubscribed = u.IsSubscribed;
                    _appUsersRepository.Update(user);
                    result.IsSuccess = true;
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                //Result - ProcessViewModel
                result.IsSuccess = true;
                result.Message = string.Format(SiteConstants.Update_Failure, "Password");
            }
            return result;
        }

        #endregion Update password

        #region Forgot Password

        [UnitOfWork]
        public ProcessViewModel ForgotPassword(string email)
        {
            ProcessViewModel result = new ProcessViewModel();
            try
            {
                var user = _appUsersRepository.Get(x => x.Email == email);
                if (user != null)
                {
                    user.IsResetPassword = false;
                    var sendemail = _emailTemplateService.ProcessedToEmailForgotPassword(user);

                    result.IsSuccess = true;
                    result.Message = string.Format(SiteConstants.ForgotPassword_Success);
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = string.Format(SiteConstants.Update_Failure, "Password");
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = string.Format(SiteConstants.Update_Failure, "Password");
            }

            return result;
        }

        #endregion ForgotPassword

        #region Reset Password

        [UnitOfWork]
        public ProcessViewModel ResetPassword(ResetPasswordViewModel model)
        {
            ProcessViewModel result = new ProcessViewModel();
            try
            {
                var user = _appUsersRepository.Get(x => x.UserGuid == model.uid);
                if (user != null)
                {
                    if (user.IsResetPassword)
                    {
                        //Result - ProcessViewModel
                        result.IsSuccess = false;
                        result.Message = string.Format(SiteConstants.ResetPassword_AlreadyResetPassword);
                    }
                    else
                    {
                        user.Password = CryptoServiceProvider.EncodePassword(model.RPassword, user.Id.ToString());
                        user.IsResetPassword = true;
                        //Call Update function
                        _appUsersRepository.Update(user);

                        //Result - ProcessViewModel
                        result.IsSuccess = true;
                        result.Message = string.Format(SiteConstants.Update_Success, "Password");
                    }
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = string.Format(SiteConstants.Update_Failure, "Password");
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = string.Format(SiteConstants.Update_Failure, "Password");
            }

            return result;
        }

        #endregion Reset Password

        #region Update User Status && Locked/UnLocked

        [UnitOfWork]
        public ProcessViewModel UpdateUserStatus(string UserGuid, string Status)
        {
            Guid uid = new Guid(UserGuid);
            bool status = Status == "1" ? true : false;
            bool IsEmailConfirmed = Status == "1" ? true : false;
            ProcessViewModel result = new ProcessViewModel();
            try
            {
                var user = _appUsersRepository.Get(x => x.UserGuid == uid);
                if (user != null)
                {
                    user.IsActive = !status;
                    user.IsEmailConfirmed = !status;

                    //Call Update function
                    _appUsersRepository.Update(user);

                    //Result - ProcessViewModel
                    result.IsSuccess = true;
                    result.Message = string.Format(SiteConstants.Update_Success, "User Status");
                    _dealerCarService.Getnerbyzip("10", user);
                    _dealerCarService.Getnerbyzip("20", user);
                    _dealerCarService.Getnerbyzip("30", user);
                    _dealerCarService.Getnerbyzip("40", user);
                    _dealerCarService.Getnerbyzip("50", user);
                    _dealerCarService.Getnerbyzip("75", user);
                    _dealerCarService.Getnerbyzip("100", user);
                    _dealerCarService.Getnerbyzip("150", user);
                    _dealerCarService.Getnerbyzip("200", user);
                    _dealerCarService.Getnerbyzip("300", user);
                    _dealerCarService.Getnerbyzip("500", user);
                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = string.Format(SiteConstants.Update_Failure, "User Status");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                result.IsSuccess = false;
                result.Message = string.Format(SiteConstants.Update_Failure, "User Status");
            }

            return result;
        }

        [UnitOfWork]
        public ProcessViewModel UpdateUserLockoutStatus(string UserGuid, string Status)
        {
            Guid uid = new Guid(UserGuid);
            bool status = Status == "1" ? true : false;
            ProcessViewModel result = new ProcessViewModel();
            try
            {
                var user = _appUsersRepository.Get(x => x.UserGuid == uid);
                if (user != null)
                {
                    user.IsLockout = !status;
                    //Call Update function
                    _appUsersRepository.Update(user);

                    //Result - ProcessViewModel
                    result.IsSuccess = true;
                    if (!status)
                        result.Message = string.Format(SiteConstants.Update_Success, "User locked");
                    else
                        result.Message = string.Format(SiteConstants.Update_Success, "User unlocked");


                }
                else
                {
                    result.IsSuccess = false;
                    result.Message = string.Format(SiteConstants.Update_Failure, "User Status");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                result.IsSuccess = false;
                result.Message = string.Format(SiteConstants.Update_Failure, "User Status");
            }

            return result;
        }


        public List<CarOrderDetail> GetCarOrderDetails(int buyerId)
        {
            // var query = this._orderDetailRepository.GetAll(c => c.BuyerID == buyerId ||c.SellerID==buyerId);
            var query = this._orderDetailRepository.GetAll(c => (c.BuyerDealer != null && c.BuyerDealer.Id == buyerId)
            || (c.SellerDealer != null && c.SellerDealer.Id == buyerId));
            return query.ToList();
        }

        public List<CarOrderOnHold> GetCarOrderOnHold(int buyerId)
        {
            //    var query = this._orderonHoldRepository.GetAll(c => c.carOrderDetail != null && c.carOrderDetail.BuyerID == buyerId);
            //    return query.ToList();

            return new List<CarOrderOnHold>();
        }

        #endregion Update User Status && Locked/UnLocked 

        #endregion Public Method

    }
}
