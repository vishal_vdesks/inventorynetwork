﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Incite.Core.Domain;
using Incite.Data;
using Incite.Data.Repositories;

namespace Incite.Services.Impl
{
    public class NotificationsService : INotificationsService
    {
        private readonly INotificationsRepository _notificationRepository;

        public NotificationsService(INotificationsRepository notificationsRepository)
        {
            _notificationRepository = notificationsRepository;
        }

        public List<Notifications> GetAllNotifications(int dealerCustomerId, bool unreadOnly=false)
        {
            var query = this._notificationRepository.GetAll();
            if (dealerCustomerId > 0)
                query = query.Where(x => x.ToDealerId == dealerCustomerId);
            if (unreadOnly)
                query = query.Where(x => x.IsRead == false);

            return query.ToList();
        }

        [UnitOfWork]
        public void InsertNotification(Notifications notification)
        {
            this._notificationRepository.Insert(notification);
        }

        public void MarkNotificationRead(int dealerCustomerId)
        {
            var notifications = this.GetAllNotifications(dealerCustomerId,true);
            foreach (var item in notifications)
            {
                item.IsRead = true;
                this._notificationRepository.Update(item);
            }
        }
    }
}
