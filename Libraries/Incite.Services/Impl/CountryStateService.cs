﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Incite.Core.Domain;
using Incite.Data.Repositories;

namespace Incite.Services.Impl
{
    public class CountryStateService : ICountryStateService
    {
        private readonly ICountryRepository _countryRepo;
        private readonly IStateRepository _stateRepo;
        public CountryStateService(ICountryRepository countryRepo, IStateRepository stateRepo)
        {
            _countryRepo = countryRepo;
            _stateRepo = stateRepo;
        }

        public List<Country> GetAllCountry()
        {
            var query = _countryRepo.GetAll();
            return query.ToList();
        }

        public List<State> GetAllState()
        {
            return _stateRepo.GetAll().ToList();
        }
    }
}
