﻿
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System.IO;

namespace Incite.Services.Impl
{
    class AWS3Helper
    {
        public bool createFolder(string bucketName, string completePath, string folderName)
        {
            IAmazonS3 client = new AmazonS3Client(RegionEndpoint.USEast2);

            var listResponse = client.ListObjectsV2(new ListObjectsV2Request
            {
                BucketName = bucketName,
                Prefix = completePath + folderName
            });


            if (listResponse.S3Objects.Count <= 0)
            {
                TransferUtility utility = new TransferUtility(client);
                TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();
                request.BucketName = bucketName + completePath;
                request.Key = folderName + @"/";
                request.InputStream = new MemoryStream();
                utility.Upload(request);
            }

            return true;
        }


        public bool uplaodFile(string bucketName, string Key, Stream fileStram)
        {
            IAmazonS3 client = new AmazonS3Client(RegionEndpoint.USEast2);
            TransferUtility utility = new TransferUtility(client);
            TransferUtilityUploadRequest request = new TransferUtilityUploadRequest();
            request.BucketName = bucketName;
            request.Key = Key;
            request.InputStream = fileStram;
            utility.Upload(request);
            return true;
        }


        public bool downlaodFromBucket(string bucketName, string subDirectoryInBucket, string fileName, string pathToSave)
        {
            try
            {
                IAmazonS3 client = new AmazonS3Client(RegionEndpoint.USEast2);
                TransferUtility utility = new TransferUtility(client);
                if (!string.IsNullOrEmpty(subDirectoryInBucket))
                    bucketName = bucketName + @"/" + subDirectoryInBucket;
                string filename = pathToSave + "\\" + fileName;
                FileStream fs = File.Create(filename);
                fs.Close();
                utility.Download(filename, bucketName, fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
