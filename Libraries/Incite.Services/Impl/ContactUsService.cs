﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Incite.Core.Domain;
using Incite.Core.Infrastructure;
using Incite.Data;
using Incite.Data.Repositories;
using Incite.Services.ViewModels;

namespace Incite.Services.Impl
{
    public class ContactUsService : IContactUsService
    {
        private readonly IContactUsRepository _contactUsRepository;

        public ContactUsService(IContactUsRepository contactUsRepository)
        {
            _contactUsRepository = contactUsRepository;
        }

        public List<ContactUs> GetAllContactUs()
        {
            var query = this._contactUsRepository.GetAll();

            return query.ToList();
        }

        [UnitOfWork]
        public ProcessViewModel InsertContactUs(ContactUs contactUs)
        {
            var response = new ProcessViewModel();
            try
            {
                var request = HttpContext.Current.Request;

                contactUs.IPAddres = request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? request.UserHostAddress;

                this._contactUsRepository.Insert(contactUs);
                response.IsSuccess = true;
                response.Message = string.Format(SiteConstants.Insert_Success,"Contact");
            }
            catch(Exception ex)
            {
                response.IsSuccess = false;
                response.Message = string.Format(SiteConstants.Insert_Failure, "Contact");
            }

            return response;
        }
      
    }
}
