﻿using Incite.Core.Domain;
using Incite.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Incite.Services
{
    public interface IUsersService
    {
        List<AppUsers> GetAllUsers();
        //void InsatllRecored();

        ProcessViewModel NewUser(RegisterViewModel model);
        UserViewModel ValidateUser(string email, string Password);
        bool ConfirmEmail(Guid uid);

        UserProfileViewModel GetUserProfile(int currentUSerId);
        AppUsers GetAppUser(int appuserId);

        ProcessViewModel UpdateUserProfile(UserProfileViewModel model, int currentUSerId);

        ProcessViewModel UpdatePassword(UpdatePasswordViewModel model, int currentUSerId);

        ProcessViewModel UpdateUser(AppUsers u);

        ProcessViewModel ForgotPassword(string email);

        ProcessViewModel ResetPassword(ResetPasswordViewModel model);

        ProcessViewModel UpdateUserStatus(string userguid, string status);

        ProcessViewModel UpdateUserLockoutStatus(string userguid, string status);



        List<Country> GetAllCountry();
        Country GetCountryById(int Id);
        List<State> GetAllState();


        List<CarOrderDetail> GetCarOrderDetails(int buyerId);
        List<CarOrderOnHold> GetCarOrderOnHold(int buyerId);
    }
}
