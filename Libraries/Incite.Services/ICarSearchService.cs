﻿using Incite.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services
{
    public interface ICarSearchService
    {
        CarFilterViewModel PrepareHomeCareFilter(CarFilterViewModel request = null);
        CarFilterViewModel PrepareSortby(CarFilterViewModel model = null);
    }
}
