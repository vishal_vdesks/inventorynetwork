﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services
{
    public interface IImportExportService
    {
         Task<bool> ImportCarDetails(String Path);
    }
}
