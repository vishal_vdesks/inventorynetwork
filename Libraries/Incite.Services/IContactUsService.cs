﻿using Incite.Core.Domain;
using Incite.Services.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services
{
    public interface IContactUsService
    {
        ProcessViewModel InsertContactUs(ContactUs contactUs);
        List<ContactUs> GetAllContactUs();
    }
}
