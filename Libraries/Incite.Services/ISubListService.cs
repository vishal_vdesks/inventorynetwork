﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services
{
    public interface ISubListService
    {
        void InsertSubList(SubList subList);
        List<SubList> GetAllSubList();
    }
    
}
