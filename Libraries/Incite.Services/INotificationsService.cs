﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services
{
   public interface INotificationsService
    {
        void InsertNotification(Notifications notification);
        List<Notifications> GetAllNotifications(int dealerCustomerId, bool unreadOnly = false);
        void MarkNotificationRead(int dealerCustomerId);
    }
}
