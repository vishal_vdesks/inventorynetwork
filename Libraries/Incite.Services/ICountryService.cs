﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services
{
    public interface ICountryStateService
    {
        List<Country> GetAllCountry();
        List<State> GetAllState();
    }
}
