﻿using Incite.Core.Domain;
using Incite.Services.ViewModels;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services
{
    public interface IPaymentService
    {
        #region Stripe's Create/Get Customer Account
        ProcessViewModel CreateCustomer(UpdateSettingsViewModel model, string stripeCustomerId = "");
        Customer GetCustomer(string stripeCustomerId);
        ProcessViewModel UpdateCustomerDefaultPaymentSource(Customer customer, string cardId);
        ProcessViewModel CreateCustomer(CustomerPaymentInfo model, string accountType = "card");
        ProcessViewModel VerifyCustomer(string custId, int AppUserId, VerifyBankAccount model);
        StripeList<IPaymentSource> GetUserPaymentSourceList(string UserEmail);
        #endregion

        #region Stripe's Create/Get Connect Account
        StripeList<IExternalAccount> GetConnectUserPaymentSourceList(string UserEmail);
        ProcessViewModel CreateConnect(AddConnectAccountsViewModel model);
        #endregion

        #region Common Methods
        List<CustomerPaymentTypes> GetCustomerPaymentTypes(int BuyerId);
        List<IPaymentSource> GetCustomerPaymentTypes(string stripeCustomerId);

        ProcessViewModel ProcessBuyNowHoldNow(BuyNowHoldNowDetailViewModel model);
        #endregion


        #region Stripe's Plan List

        StripeList<Plan> GetPlanList();
        Plan GetPlan(string Id);

        ProcessViewModel CreateSubscription(string planId, string customerId, AppUsers user, Plan planInfo, bool isReturning);


        ProcessViewModel AddCreditCard(CustomerPaymentInfo model, string stripecustomerId);

        Card GetCard(string stripeCustomerId, string cardId);

        ProcessViewModel UpdateCard(CustomerPaymentInfo model, string stripeCustmerId, string cardId);
        ProcessViewModel DeleteCard(string stripeCustmerId, string cardId);

        ProcessViewModel CancelSubscription(string subscriptionId);
        ProcessViewModel ReSubscription(string subscriptionId);

        StripeList<Charge> GetCharges(string stripeCustomerId);

        #endregion
    }
}
