﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Incite.Services.ViewModels
{
    public class UpdateSettingsViewModel
    {
        public UpdateSettingsViewModel()
        {

        }
        public int AppUserId { get; set; }
        public UserProfile Userprofile { get; set; }
        public Stripe.BankAccountOptions BankAccount { get; set; }
        [Required(ErrorMessage = "Account Holder Name is required")]
        public string AccountHolderName { get; set; }
        [Required(ErrorMessage = "Account Holder Type is required")]
        public string AccountHolderType { get; set; }


        [Required(ErrorMessage = "Account Holder Number is required")]
        [StringLength(16, ErrorMessage = "Must be between 6 and 16 digits", MinimumLength = 6)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Account number must be numeric")]
        [DataType(DataType.Password)]
        public string AccountNumber { get; set; }

        [Required(ErrorMessage = "Confirm Account Number is required")]
        [StringLength(16, ErrorMessage = "Must be between 6 and 25 characters", MinimumLength = 6)]
        [System.ComponentModel.DataAnnotations.Compare("AccountNumber")]
        public string ConfirmAccountNumber { get; set; }

        [Required(ErrorMessage = "Country is required")]
        public string Country { get; set; }
        [Required(ErrorMessage = "Currency is required")]
        public string Currency { get; set; }
        [Required(ErrorMessage = "Routing Number is required")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Routing number must be numeric")]
        [StringLength(9, ErrorMessage = "Routing number must be 9 digits", MinimumLength = 9)]
        public string RoutingNumber { get; set; }
        public string CustomerEmailId { get; set; }
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }
        public string Message { get; set; }
    }



    public class CustomerPaymentInfo
    {
        public CustomerPaymentInfo()
        {
           
        }

        public string AccountType { get; set; }

        public string AccountHolderName { get; set; }

        public string RoutingNumber { get; set; }

        public string AccountNumber { get; set; }

        public int AppUserId { get; set; }

        public string CustomerEmailId { get; set; }

        //[Required]
        [Display(Name = "First Name")]
        //[Required(ErrorMessage = "Please provide first name")]
        public string FirstName { get; set; }

        //[Required]
        [Display(Name = "Last Name")]
        //[Required(ErrorMessage = "Please provide last name")]
        public string LastName { get; set; }

        [Display(Name = "Name On Card")]
        [Required(ErrorMessage = "Please provide name on card")]
        public string NameOnCard { get; set; }

        // [Required(AllowEmptyStrings = false)]
        [Display(Name = "Card Number")]
        [Required(ErrorMessage = "Please provide card number")]
        public string CardNumber { get; set; }

        [Display(Name = "Card Type")]
        public long CardTypeId { get; set; }

        [Display(Name = "payment Mode")]
        public long PaymentModeId { get; set; }


        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Expiry Month")]
        public string ExpiryMonth { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Expiry Year")]
        public string ExpiryYear { get; set; }

        public string Description { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage ="Please provide cvv")]
        [Display(Name = "CVV Number")]
        public string CVVNumber { get; set; }

        [Display(Name = "Address 1")]
        public string Address1 { get; set; }

        public long Amount { get; set; }

        [Display(Name = "Address 2")]
        public string Address2 { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Zip { get; set; }


        [Display(Name = "Auth Code")]
        public string AuthCode { get; set; }


        //[Required(AllowEmptyStrings = true)]
        [Display(Name = "Subscription Id")]
        public string SubscriptionId { get; set; }

        //[Required(AllowEmptyStrings = true)]
        [Display(Name = "Transaction Id")]
        public string TransactionId { get; set; }

        //[Required(AllowEmptyStrings = true)]
        [Display(Name = "Subscription Start Date")]
        public DateTime SubscriptionStartDate { get; set; }

        //[Required(AllowEmptyStrings = true)]
        [Display(Name = "Subscription Type")]
        public string SubscriptionType { get; set; }

        /// <summary>
        /// display bank option or not.
        /// </summary>
        public bool DisplayBankInfoPanel { get; set; }
        public string PhoneNumber { get; set; }
        public string CountryName { get; set; }
        public string PlanId { get; set; }
    }
}
