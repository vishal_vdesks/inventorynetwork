﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services.ViewModels
{
    public class VinApiViewModel
    {
        public string Make { get; set; }
        public string Model { get; set; }
        public string ModelYear { get; set; }
        public string Series { get; set; }

        public int MakerId { get; set; }
        public int ModelId { get; set; }
        public int YearId { get; set; }
        public int TrimId { get; set; }
        public int EngineId { get; set; }

    }
}
