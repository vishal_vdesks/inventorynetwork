﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Services.ViewModels
{
    public class UserViewModel
    {
        public UserViewModel()
        {
            Process = new ProcessViewModel();
            user = new AppUsers();
        }
        public AppUsers user { get; set; }
        public ProcessViewModel Process { get; set; }
    }
}
