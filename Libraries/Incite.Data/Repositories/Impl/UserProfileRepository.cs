﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Incite.Core.Domain;

namespace Incite.Data.Repositories
{
    public class UserProfileRepository : RepositoryBase<UserProfile, int>, IUserProfileRepository
    {
    }
}
