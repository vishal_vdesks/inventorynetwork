﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Repositories.Impl
{
    public class NotificationsRepository: RepositoryBase<Notifications, int>, INotificationsRepository
    {
    }
}
