﻿using Incite.Core.Domain;

namespace Incite.Data.Repositories
{
    public interface IDealerMessagesRepository : IRepository<DealerMessages, int>
    {
    }
}
