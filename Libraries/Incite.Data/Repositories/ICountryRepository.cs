﻿using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Repositories
{
    public interface ICountryRepository : IRepository<Country, int>
    {
    }

    public interface IStateRepository : IRepository<State, int>
    {
    }
}
