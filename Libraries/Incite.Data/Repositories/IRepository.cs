﻿using Incite.Core;
using System.Linq;

namespace Incite.Data.Repositories
{
    /// <summary>
    /// This interface must be implemented by all repositories to ensure UnitOfWork to work.
    /// </summary>
    public interface IRepository
    {

    }
    /// <summary>
    /// This interface is implemented by all repositories to ensure implementation of fixed methods.
    /// </summary>
    /// <typeparam name="TEntity">Main Entity type this repository works on</typeparam>
    /// <typeparam name="TPrimaryKey">Primary key type of the entity</typeparam>
    public interface IRepository<TEntity, TPrimaryKey> : IRepository where TEntity : Entity<TPrimaryKey>
    {
        /// <summary>
        /// Used to get a IQueryable that is used to retrive entities from entire table.
        /// </summary>
        /// <returns>IQueryable to be used to select entities from database</returns>
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// Used to get a IQueryable that is used to retrive entities from entire table with where condition.
        /// </summary>
        /// <param name="expression">Condition</param>
        /// <returns>IQueryable to be used to select entities from database</returns>
        IQueryable<TEntity> GetAll(System.Linq.Expressions.Expression<System.Func<TEntity, bool>> expression);

        /// <summary>
        /// Gets an entity.
        /// </summary>
        /// <param name="key">Primary key of the entity to get</param>
        /// <returns>Entity</returns>
        TEntity Get(TPrimaryKey key);

        /// <summary>
        ///  Gets an entity with Where condition.
        /// </summary>
        /// <param name="expression">Condition</param>
        /// <returns>Entity</returns>
        TEntity Get(System.Linq.Expressions.Expression<System.Func<TEntity, bool>> expression);

        /// <summary>
        /// Inserts a new entity.
        /// </summary>
        /// <param name="entity">Entity</param>
        void Insert(TEntity entity);
        /// <summary>
        /// If recored alreday avelabule then update data
        /// if not then insert new recored
        /// </summary>
        /// <param name="entity"></param>
        void InsertOrUpdate(TEntity entity);

        /// <summary>
        /// Updates an existing entity.
        /// </summary>
        /// <param name="entity">Entity</param>
        void Update(TEntity entity);

        /// <summary>
        /// Deletes an entity.
        /// </summary>
        /// <param name="id">Id of the entity</param>
        void Delete(TPrimaryKey id);
    }

}
