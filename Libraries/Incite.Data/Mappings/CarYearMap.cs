﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    public class CarYearMap:ClassMap<CarYear>
    {
        public CarYearMap()
        {
            Table("CarYear");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Name).Column("Name");
           // HasMany(x => x.carDetails).KeyColumn("CarYearId");
        }

    }
}
