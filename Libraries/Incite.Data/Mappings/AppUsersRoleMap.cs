﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    public class AppUsersRoleMap : ClassMap<AppUsersRole>
    {
        public AppUsersRoleMap()
        {
            Table("AppUsersRole");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.RoleName).Column("RoleName");
            Map(x => x.Description).Column("Description");
            HasManyToMany(x => x.Users).Cascade.All().Table("AppUsersInRole");

        }

        
    }
}
