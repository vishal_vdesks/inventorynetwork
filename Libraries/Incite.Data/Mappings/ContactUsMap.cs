﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    public class ContactUsMap : ClassMap<ContactUs>
    {
        public ContactUsMap()
        {
            Table("ContactUs");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.FullName).Column("FullName");
            Map(x => x.EmailAddress).Column("EmailAddress");
            Map(x => x.Message).Column("Message");
            Map(x => x.PhoneNumber).Column("PhoneNumber");
            Map(x => x.IPAddres).Column("IPAddres");
        }
    }
}
