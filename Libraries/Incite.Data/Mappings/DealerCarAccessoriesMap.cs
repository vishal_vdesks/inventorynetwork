﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate;
using FluentNHibernate.Mapping;
using Incite.Core.Domain;

namespace Incite.Data.Mappings
{
    public class DealerCarAccessoriesMap : ClassMap<DealerCarAccessories>
    {
        public DealerCarAccessoriesMap()
        {
            Table("DealerCarAccessories");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.IsAccessoriesAvailable).Column("IsAccessoriesAvailable");

            //mapping
            
            References(x => x.dealerCarDetails).Cascade.All().Column("DealerCarDetailsId").ForeignKey("FK_DealerCarAccessories_DealerCarDetails");
            References(x => x.carAccessories).Cascade.All().Column("CarAccessoriesId").ForeignKey("FK_DealerCarAccessories_CarAccessories");
            
            


        }
       
    }
}
