﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    public class AuditLogMap : ClassMap<AuditLog>
    {
        public AuditLogMap()
        {
            Table("AuditLog");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Username).Column("Username").CustomSqlType("nvarchar(MAX)");
            Map(x => x.AuditEntryType).Column("AuditEntryType").CustomSqlType("nvarchar(MAX)");
            Map(x => x.EntityFullName).Column("EntityFullName").CustomSqlType("nvarchar(MAX)");
            Map(x => x.EntityShortName).Column("EntityShortName").CustomSqlType("nvarchar(MAX)");
            Map(x => x.EntityId).Column("EntityId");
            Map(x => x.FieldsName).Column("FieldsName").CustomSqlType("nvarchar(MAX)");
            Map(x => x.FieldsValue).Column("FieldsValue").CustomSqlType("nvarchar(MAX)");
            Map(x => x.Timestamp).Column("Timestamp");
        }
    }
}
