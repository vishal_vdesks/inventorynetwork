﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Incite.Core.Domain;

namespace Incite.Data.Mappings
{
    public class CarOrderDetailMap: ClassMap<CarOrderDetail>
    {
        public CarOrderDetailMap()
        {
            Table("CarOrderDetail");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");

            Map(x => x.TransactionType).Column("Transaction_Type");
            Map(x => x.PaymentStatus).Column("PaymentStatus");
            Map(x => x.TransactionId).Column("TransactionId");
            Map(x => x.IsOnHold).Column("IsOnHold");
            Map(x => x.CreatedOn).Column("CreatedOn");
            Map(x => x.Amount).Column("Amount");
            Map(x => x.SellerDeposit).Column("SellerDeposit");
            Map(x => x.BuyerCharge).Column("BuyerCharge");
            

            //mapping
            References(x => x.dealerCarDetails).Cascade.All().Column("DealerCarDetailId").ForeignKey("FK_CarOrderDetails_DealerCarDetails");
            References(x => x.BuyerDealer).Cascade.All().Column("BuyerID").ForeignKey("FK_CarOrderDetails_BuyerDealer");
            References(x => x.SellerDealer).Cascade.All().Column("SellerID").ForeignKey("FK_CarOrderDetails_SellerDealer");

        }
    }
}
