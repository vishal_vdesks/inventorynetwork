﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;

namespace Incite.Data.Mappings
{
    public class DealerMessagesMap : ClassMap<DealerMessages>
    {
        public DealerMessagesMap()
        {
            Table("DealerMessages");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");

            Map(x => x.Message).Column("Message");
            Map(x => x.CreatedTs).Column("CreatedTs");            

            //mapping
            References(x => x.fromDealer).Cascade.All().Column("FromDealerId").ForeignKey("FK_DealerMessages_fromDealer");
            References(x => x.toDealer).Cascade.All().Column("ToDealerId").ForeignKey("FK_DealerMessages_toDealer");            
        }
    }
}
