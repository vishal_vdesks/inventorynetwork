﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    class EngineTypeMap : ClassMap<EngineType>
    {
        public EngineTypeMap()
        {
            Table("EngineType");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.EngineTypeName).Column("EngineTypeName");
           // HasMany(x => x.dealerCarDetails).KeyColumn("EngineTypeId");
        }
    }
}
