﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    public class DealerCarImagesMap : ClassMap<DealerCarImages>
    {
        public DealerCarImagesMap()
        {
            Table("DealerCarImages");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.NormalImagePath).Column("NormalImagePath");
            Map(x => x.ThumbImagePath).Column("ThumbImagePath");
            Map(x => x.LargeImagePath).Column("LargeImagePath");
            Map(x => x.IsDeleted).Column("IsDeleted");

            //mapping

            References(x => x.dealerCarDetails).Cascade.All().Column("DealerCarDetailsId").ForeignKey("FK_DealerCarImages_DealerCarDetails");

        }

    }
}
