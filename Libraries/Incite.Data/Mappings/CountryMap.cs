﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Incite.Core.Domain;
using FluentNHibernate;
using FluentNHibernate.Mapping;
namespace Incite.Data.Mappings
{
    public class CountryMap : ClassMap<Country>
    {
        public CountryMap()
        {
            Table("Country");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Name).Column("Name");
            Map(x => x.TwoLettersIsoCode).Column("TwoLettersIsoCode");
            Map(x => x.ThreeLettersIsoCode).Column("ThreeLettersIsoCode");
            Map(x => x.DisplayOrder).Column("DisplayOrder");
            Map(x => x.ImageIcon).Column("ImageIcon");
            Map(x => x.CreatedDate).Column("CreatedDate");
        }

    }


    public class StateMap : ClassMap<State>
    {
        public StateMap()
        {
            Table("Us_State");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Name).Column("Name");
        }
    }
}
