﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Incite.Core.Domain;

namespace Incite.Data.Mappings
{
    public class SubListMap: ClassMap<SubList>

    {
        public SubListMap()
        {
            Table("SubList");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.EmailId).Column("Email");
        }
    }
}
