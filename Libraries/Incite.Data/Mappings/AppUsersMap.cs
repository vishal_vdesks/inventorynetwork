﻿using FluentNHibernate;
using FluentNHibernate.Mapping;
using Incite.Core.Domain;

namespace Incite.Data.Mappings
{
    public class AppUsersMap : ClassMap<AppUsers>
    {

        public AppUsersMap()
        {
            Table("AppUsers");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.UserName).Column("UserName");
            Map(x => x.Password).Column("Password");
            Map(x => x.Email).Column("Email");
            Map(x => x.IsEmailConfirmed).Column("IsEmailConfirmed");
            Map(x => x.IsActive).Column("IsActive");
            Map(x => x.IsLockout).Column("IsLockout");
            Map(x => x.IsResetPassword).Column("IsResetPassword");
            Map(x => x.LockoutDate).Column("LockoutDate");
            Map(x => x.UserGuid).Column("UserGuid").Unique();


            Map(x => x.IsSubscribed).Column("IsSubscribed");
            Map(x => x.SubscriptionId).Column("SubscriptionId");
            Map(c => c.StripecustomerId).Column("StripecustomerId");
            Map(c => c.PlanId).Column("PlanId");
            Map(c => c.SubscribedUpto).Column("SubscribedUpto");

            //Relationship
            HasManyToMany(x => x.UsersRoles).Cascade.All().Table("AppUsersInRole");
            HasOne(x => x.UserProfile).ForeignKey("AppUserId");
            HasMany(x => x.DealerComments).KeyColumn("RatingForDealerId");
            HasOne(x => x.dealerCustomerCreation).ForeignKey("AppUserId");
            //HasOne(x => x.dealerConnectCreation).ForeignKey("AppUserId");
            HasMany(x => x.dealerConnectCreation).KeyColumn("AppUserId");

        }
    }
}