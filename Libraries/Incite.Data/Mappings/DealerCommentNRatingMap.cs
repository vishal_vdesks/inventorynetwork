﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    public class DealerCommentNRatingMap : ClassMap<DealerCommentNRating>
    {
        public DealerCommentNRatingMap()
        {
            Table("DealerCommentNRating");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");

            Map(x => x.Comment).Column("Comment");
            Map(x => x.CommentReply).Column("CommentReply");
            Map(x => x.Rating).Column("Rating");
            Map(x => x.CreatedTs).Column("CreatedTs");            

            //mapping
            References(x => x.appUsers).Cascade.All().Column("DealerId").ForeignKey("FK_DealerCommentNRating_AppUsers");
            References(x => x.ratingUsers).Cascade.All().Column("RatingForDealerId").ForeignKey("FK_DealerCommentNRating_RatingUsers");            
        }
    }
}
