﻿using FluentNHibernate;
using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Incite.Data.Mappings
{
    public class CarAccessoriesMap : ClassMap<CarAccessories>
    {
        public CarAccessoriesMap()
        {
            Table("CarAccessories");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.Accessories).Column("Accessories");

            
        }
    }
}
