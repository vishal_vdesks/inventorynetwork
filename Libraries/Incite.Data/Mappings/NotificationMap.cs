﻿using FluentNHibernate.Mapping;
using Incite.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Data.Mappings
{
    public class NotificationMap : ClassMap<Notifications>
    {
        public NotificationMap()
        {
            Table("Notifications");
            LazyLoad();
            Id(x => x.Id).GeneratedBy.Identity().Column("Id");
            Map(x => x.RowId).Column("RowId");
            Map(x => x.CreatedOn).Column("CreatedOn");
            Map(x => x.FromDealerId).Column("FromDealerId");
            Map(x => x.ToDealerId).Column("ToDealerId");
            Map(x => x.IsRead).Column("IsRead");

            Map(x => x.Description).Column("Description");
            Map(x => x.Type).Column("Type");
            Map(x => x.PageDataId).Column("PageDataId");
            Map(x => x.IsOpen).Column("IsOpen");
        }
    }
}
