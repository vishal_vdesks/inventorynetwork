﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
   public class Notifications : Entity<int>
    {
        public virtual int RowId { get; set; }

        public virtual DateTime CreatedOn { get; set; }

        public virtual int FromDealerId { get; set; }

        public virtual int ToDealerId { get; set; }

        public virtual bool IsRead { get; set; }

        public virtual string Description { get; set; }

        public virtual string Type { get; set; }

        /// <summary>
        /// from which page msg is sent. like if car details page, this will be car detail id
        /// </summary>
        public virtual string PageDataId { get; set; }

        public virtual bool IsOpen { get; set; }
    }
}
