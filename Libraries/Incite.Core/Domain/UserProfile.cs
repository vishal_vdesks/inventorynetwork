﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class UserProfile : Entity<int>
    {
        public UserProfile()
        {
        }
        public virtual int AppUserId { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string MobileNumber { get; set; }
        public virtual string AdditionalMobileNumber { get; set; }
        public virtual string AdditionalEmail { get; set; }
        public virtual string FAX { get; set; }
        public virtual string AdminName { get; set; }
        public virtual int State { get; set; }
        public virtual string ProfileImage { get; set; }
        public virtual string ProfileThmbnailImage { get; set; }
        public virtual DateTime CreateDate { get; set; }
        public virtual DateTime? LastLoginDate { get; set; }
        public virtual AppUsers User { get; set; }

        public virtual string DealershipAddressLine1 { get; set; }
        public virtual string DealershipAddressLine2 { get; set; }
        public virtual string Zipcode { get; set; }
        public virtual string City { get; set; }
        public virtual string LicenseNumber { get; set; }

    }
}
