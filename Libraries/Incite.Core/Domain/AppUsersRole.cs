﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class AppUsersRole: Entity<int>
    {
        public AppUsersRole()
        {
            Users = new List<AppUsers>();
        }

        public virtual string RoleName { get; set; }
        public virtual string Description { get; set; }

        public virtual IList<AppUsers> Users { get; set; }

    }
}
