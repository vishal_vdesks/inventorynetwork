﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class ContactUs :Entity<int>
    {

        public virtual string FullName { get; set; }
        public virtual string EmailAddress { get; set; }
        public virtual string PhoneNumber { get; set; }

        public virtual string Message { get; set; }
        public virtual string IPAddres { get; set; }

       
    }
}

