﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class CarModel : Entity<int>
    {
        public CarModel()
        {
            carDetails = new List<CarDetails>();
        }
        public virtual string Name { get; set; }

        public virtual IList<CarDetails> carDetails { get; set; }


    }
}
