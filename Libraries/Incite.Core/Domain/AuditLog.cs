﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class AuditLog : Entity<int>
    {
        public virtual string Username { get; set; }
        public virtual string AuditEntryType { get; set; }
        public virtual string EntityFullName { get; set; }
        public virtual string EntityShortName { get; set; }
        public virtual int? EntityId { get; set; }
        public virtual string FieldsName { get; set; }
        public virtual string FieldsValue { get; set; }
        public virtual DateTime? Timestamp { get; set; }
    }
}
