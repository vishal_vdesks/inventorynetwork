﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class CarOrderOnHold : Entity<int>
    {
        public CarOrderOnHold()
        {
            carOrderDetail = new CarOrderDetail();
        }

        public virtual int CarOrderDetailId { get; set; }
        public virtual decimal PartialAmountPaid { get; set; }
        public virtual decimal RestAmount { get; set; }
        public virtual DateTime TimeToHold { get; set; }

        public virtual CarOrderDetail carOrderDetail { get; set; }

    }
}
