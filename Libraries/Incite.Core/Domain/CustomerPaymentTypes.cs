﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class CustomerPaymentTypes:Entity<int>
    {
        public CustomerPaymentTypes()
        {
            DealerCustomerCreation = new DealerCustomerCreation();
        }
        public virtual DealerCustomerCreation DealerCustomerCreation { get; set; }

        public virtual int DealerCustId { get; set; }
        public virtual string PaymentId { get; set; }
        public virtual string PaymentType { get; set; } //bank,credit
        public virtual string StripeId { get; set; }
        public virtual bool IsVerified { get; set; }
    }
}
