﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public  class FuelType : Entity<int>
    {
        public FuelType()
        {
            carDetails = new List<CarDetails>();
        }
        
        public virtual string FuelTypeName { get; set; }

        public virtual IList<CarDetails> carDetails { get; set; }
    }
}
