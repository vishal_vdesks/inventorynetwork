﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
   public class Country : Entity<int>
    {
       // public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string TwoLettersIsoCode { get; set; }
        public virtual string ThreeLettersIsoCode { get; set; }
        public virtual int DisplayOrder { get; set; }
        public virtual string ImageIcon { get; set; }
        public virtual DateTime CreatedDate { get; set; }
    }


    public class State : Entity<int>
    {
        // public virtual int Id { get; set; }
        public virtual string Name { get; set; }
       
    }
}
