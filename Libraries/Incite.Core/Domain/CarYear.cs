﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class CarYear : Entity<int>
    {
        public CarYear()
        {
            carDetails = new List<CarDetails>();
        }
        public virtual int Name { get; set; }

        public virtual IList<CarDetails> carDetails { get; set; }
    }
}
