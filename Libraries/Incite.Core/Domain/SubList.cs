﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
   public class SubList : Entity<int>
    {
        public SubList()
        {
            subLists = new List<SubList>();
        }
        //public virtual int Id { get; set; }

        public virtual string EmailId { get; set; }

        public virtual IList<SubList> subLists { get; set; }
    }
}
