﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Incite.Core.Domain
{
    public class CarAccessories : Entity<int>
    {
        public CarAccessories()
        {
            DealerCarAccessories = new List<DealerCarAccessories>();
        }
        public virtual string Accessories { get; set; }

        public virtual IList<DealerCarAccessories> DealerCarAccessories { get; set; }

    }
}
